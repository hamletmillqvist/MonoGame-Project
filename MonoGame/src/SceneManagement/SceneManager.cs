﻿using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;
using System;
using Microsoft.Xna.Framework;
using MonoGame.src.Graphics;
using Microsoft.Xna.Framework.Input;
using MonoGame.src.Components;

namespace MonoGame.src.SceneManagement
{
    static class SceneManager
    {
        static Dictionary<string, Scene> scenes = new Dictionary<string, Scene>();
        static Scene currentScene;

        static ContentManager content;
        static GraphicsDevice graphics;
        static Game1 game;
        static SpriteBatch spriteBatch;

        public static void Initialize(Game1 runningGame, ContentManager contentManager,
            GraphicsDevice graphicsDevice)
        {
            content = contentManager;
            graphics = graphicsDevice;
            game = runningGame;

            spriteBatch = new SpriteBatch(graphics);

            lbl_DevBuild = GameObject.CreateNew("lbl_DevBuild", new Vector2(ScreenHandler.Width - 15, 20));
            lbl_DevBuild.AddComponent<Text2D>().Text = "Development Build";
            lbl_DevBuild.GetComponent<Text2D>().Align = Text2D.eAlignment.Right;
            lbl_DevBuild.GetComponent<Text2D>().Color = Color.White;

            GameObject.OnObjectCreated += GameObject_OnObjectCreated;
        }

        private static void GameObject_OnObjectCreated(GameObject sender)
        {
            if (currentScene != null)
                currentScene.AddObject(sender);
        }

        public static void AddScene(Scene scene)
        {
            if (!HasScene(scene.Name))
            {
                scenes.Add(scene.Name, scene);
            }
            else
            {
                throw new Exception(string.Format(
                    "SCENEMANAGAER: Scene '{0}' is already added.", scene.Name));
            }
        }

        public static bool HasScene(string name)
        {
            return scenes.ContainsKey(name);
        }

        public static void LoadScene(string name, bool reset = false)
        {
            GameFramework.TestInitialization();

            if (HasScene(name))
            {
                currentScene = scenes[name];
                GameConsole.PrintLine("Loaded scene \"" + currentScene.Name + "\"");

                if (!currentScene.IsStarted || reset)
                {
                    currentScene.Start();
                }
            }
            else
            {
                throw new Exception(string.Format(
                    "SCENEMANAGAER: Tried loading non-existent scene \"{0}\"", name));
            }
        }

        public static void ResetScene(string name)
        {
            GameFramework.TestInitialization();

            if (HasScene(name))
            {
                if (scenes[name].IsStarted)
                {
                    GameConsole.PrintLine("Scene \"" + name + "\" reset");
                    scenes[name].Start();
                }
                else
                {
                    GameConsole.PrintLine("Scene \"" + name + "\" hasn't been started");
                }
            }
            else
            {
                throw new Exception(string.Format(
                    "SCENEMANAGAER: Tried resetting non-existent scene \"{0}\"", name));
            }
        }

        public static List<string> AllScenes()
        {
            var list = new List<string>();

            foreach (var scene in scenes.Keys)
            {
                list.Add(scene);
            }

            return list;
        }

        public static Scene GetLoadedScene()
        {
            return currentScene;
        }

        public static void Update(GameTime gameTime)
        {
            if (Keyboard.GetState().IsKeyDown(Keys.Escape))
                game.Exit(); //DEBUG PURPOSE

            GameConsole.Update(gameTime);

            if (currentScene == null)
                throw new Exception("SCENEMANAGAER: No Scene is loaded.");

            GameCursor.Update(gameTime);
            currentScene.Update(gameTime);
        }

        static GameObject lbl_DevBuild;
        public static void Draw(GameTime gameTime)
        {
            graphics.Clear(currentScene.BackgroundColor);
            spriteBatch.Begin(SpriteSortMode.FrontToBack);
            currentScene.Draw(gameTime, spriteBatch);
            GameConsole.Draw(gameTime, spriteBatch);
            GameCursor.Draw(gameTime, spriteBatch);

            spriteBatch.DrawString(TextHandler.Default, string.Format("FPS {0}", Math.Round(1 / gameTime.ElapsedGameTime.TotalSeconds)), new Vector2(10, ScreenHandler.Height - 30), Color.LawnGreen, 0, Vector2.Zero, 1, SpriteEffects.None, 1);

            lbl_DevBuild.Draw(gameTime, spriteBatch);

            spriteBatch.End();
            currentScene.PostUpdate(gameTime);
        }
    }
}
