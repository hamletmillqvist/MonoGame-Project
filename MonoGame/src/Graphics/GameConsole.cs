﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using MonoGame.src.Components;
using MonoGame.src.SceneManagement;
using MonoGame.src.Time;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MonoGame.src.Graphics
{
    static class GameConsole
    {
        static Game1 game;
        static GraphicsDeviceManager graphics;

        static Vector2 position;
        static Vector2 size;
        static bool visible = false;
        static bool selected = true;
        static KeyboardState previousState;
        static KeyboardState currentState;
        static MouseState previousMouse;
        static MouseState currentMouse;
        static GameObject console;

        //Graphical
        static Dictionary<string, string> commands;
        static Dictionary<string, string> keyWords;
        static List<string> output = new List<string>();
        static List<Color> output_color = new List<Color>();
        static List<string> history = new List<string>();
        static string input = "";
        static string auto_fill = "";
        static int selected_history = 0;
        static bool indicator = false;
        static Timer indicator_timer = new Timer();
        static float scroll = 0;

        public static bool IsVisible { get => visible;}

        internal static void Initialize(Game1 _game, ref GraphicsDeviceManager _graphics)
        {
            game = _game;
            graphics = _graphics;

            size = new Vector2(600, 400);
            position = new Vector2(30);

            indicator_timer.OnTick += Indicator_timer_OnTick;
            indicator_timer.Time = 1;

            console = GameObject.CreateNew("console", position, size);
            console.AddComponent<BoxCollider>().Size = size;

            commands = new Dictionary<string, string>()
            {
                { "version", "Prints the games current version." },
                { "print [message]", "Prints a message in the console." },
                { "help", "Displays all avalible commands." },
                { "clear", "Clers the console." },
                { "reset", "Resets the console." },
                { "Scene.Select [gameObject]", "Selects a gameobject in the scene." },
                { "Scene.ListObjects [filter]", "Prints list of gameobjects in scene." },
                { "SceneManager.LoadScene [scene]", "Tries loading specified scene." },
                { "SceneManager.ListScenes", "Prints all avalible scenes." },
                { "exit", "Hides and resets console." },
                { "quitgame", "Exits the game." },

                { "debug.history", "Shows all previous inputted commands." },
            };

            keyWords = new Dictionary<string, string>()
            {
                { "!ROOT", "null" },
                { "!SCENE", "null" },
            };

            MediaPlayer.ActiveSongChanged += MediaPlayer_ActiveSongChanged;

            //Debug
            PerformCommand("reset");
            history.Clear();
        }

        private static void MediaPlayer_ActiveSongChanged(object sender, EventArgs e)
        {
            GameConsole.PrintLine(string.Format("Now playing: {0}", MediaPlayer.Queue.ActiveSong.Name));
        }

        private static void Indicator_timer_OnTick(GameObject sender, Timer timer)
        {
            indicator = !indicator;
        }

        public static void Show()
        {
            visible = true;
            selected = true;
            indicator_timer.Start();
        }

        public static void Hide()
        {
            visible = false;
            selected = false;
            indicator_timer.Stop();
            indicator_timer.Reset();
        }

        public static void PrintLine(string message = "", Color? color = null)
        {
            //Select inserted color, if empty set white instead.
            Color messageColor = color ?? Color.White;

            output.Add(message);
            output_color.Add(messageColor);
        }

        public static void Update(GameTime gameTime)
        {
            previousState = currentState;
            currentState = Keyboard.GetState();

            previousMouse = currentMouse;
            currentMouse = Mouse.GetState();

            if (currentState.IsKeyDown(Keys.OemPipe) &&
                previousState.IsKeyDown(Keys.OemPipe) == false)
            {
                if (IsVisible) Hide();
                else Show();
            }

            if (IsVisible)
            {
                console.GetComponent<BoxCollider>().Update(gameTime);

                if (currentMouse.LeftButton == ButtonState.Pressed && previousMouse.LeftButton != ButtonState.Pressed)
                {
                    if (console.GetComponent<BoxCollider>().Contains(currentMouse.Position))
                    {
                        selected = true;
                    }
                    else selected = false;
                }

                keyWords["!SCENE"] = SceneManager.GetLoadedScene().Name;

                if (selected)
                {
                    UpdateInput();
                    UpdateAutoFill();
                    indicator_timer.Update(gameTime);
                }
            }

            while (output.Count * 22 > (size.Y - 30) * 2.5f)
            {
                output.RemoveAt(0);
                output_color.RemoveAt(0);
            }
        }

        private static void UpdateAutoFill()
        {
            if (input.Length >= 2 || input.StartsWith("!"))
            {
                bool recommendation = false;

                foreach (var keyword in keyWords.Keys)
                {
                    var inputWord = input.Split(' ').Last();

                    if (keyword.StartsWith(inputWord.ToUpper()))
                    {
                        auto_fill = keyword.Substring(inputWord.Length);
                        recommendation = true;
                        break;
                    }
                }

                foreach (var item in commands.Keys)
                {
                    if (input.ToLower().StartsWith(item.Split(' ')[0].ToLower()))
                    {
                        if (item.EndsWith("[scene]"))
                        {
                            var scenes = SceneManager.AllScenes();
                            var words = input.Split(' ');

                            foreach (var scene in scenes)
                            {
                                if (scene.ToLower().StartsWith(words.Last().ToLower()))
                                {
                                    auto_fill = scene.Substring(words.Last().Length);
                                    recommendation = true;
                                    break;
                                }
                            }

                            if (recommendation) break;
                        }
                        else if (item.EndsWith("[gameObject]"))
                        {
                            var objects = SceneManager.GetLoadedScene().SceneObjects;
                            var words = input.Split(' ');

                            foreach (var obj in objects)
                            {
                                if (obj.Name.ToLower().StartsWith(words.Last().ToLower()))
                                {
                                    auto_fill = obj.Name.Substring(words.Last().Length);
                                    recommendation = true;
                                    break;
                                }
                            }

                            if (recommendation) break;
                        }
                    }
                    else if (item.StartsWith(input, comparisonType: StringComparison.CurrentCultureIgnoreCase))
                    {
                        var guesses = item.Split(' ')[0].Split('.');

                        if (guesses.Length > 1)
                        {
                            int count = 0;

                            foreach (var guess in guesses)
                            {
                                var words = input.Split('.');
                                var lastWord = words.Last();

                                if (guess.ToLower().StartsWith(lastWord.ToLower()) || lastWord == "")
                                {
                                    auto_fill = guess.Substring(lastWord.Length);
                                    recommendation = true;
                                    count++;

                                    //if (count < guesses.Length) auto_fill += ".";

                                    if (count == guesses.Length) auto_fill += " ";

                                    if (count == words.Length)
                                    {
                                        break;
                                    }
                                }
                                else
                                {
                                    words = input.Split(' ');

                                    auto_fill = item.Split(' ')[words.Length - 1].Substring(words.Last().Length);
                                    recommendation = true;

                                    if (words.Length - 1 < guesses.Length) auto_fill += " ";
                                    break;
                                }
                            }
                        }
                        else
                        {
                            var words = input.Split(' ');

                            auto_fill = item.Split(' ')[words.Length - 1].Substring(words.Last().Length);
                            recommendation = true;

                            if (words.Length - 1 < guesses.Length) auto_fill += " ";
                        }

                        break;
                    }
                    
                }

                if (recommendation == false)
                    auto_fill = "";
            }
            else auto_fill = "";
        }

        private static void UpdateInput()
        {
            var keys = currentState.GetPressedKeys();
            bool uppercase = false;

            if (currentState.IsKeyDown(Keys.LeftShift) || currentState.CapsLock)
                uppercase = true;

            foreach (var key in keys)
            {
                if (previousState.IsKeyDown(key) == false)
                {
                    var str = "";

                    if (key.ToString().Length == 1)
                        str = key.ToString();
                    else if (key.ToString().StartsWith("NumPad"))
                    {
                        str += key.ToString().Remove(0, key.ToString().Length - 1);
                    }
                    else
                    {
                        switch (key)
                        {
                            case Keys.Back:
                                if (input.Length > 0)
                                    input = input.Remove(input.Length - 1);
                                break;

                            case Keys.Space:
                                str = " ";
                                break;

                            case Keys.OemPeriod:
                                str = ".";
                                break;

                            case Keys.OemComma:
                                str = ",";
                                break;

                            case Keys.OemMinus:
                                if (currentState.IsKeyDown(Keys.LeftShift) || currentState.IsKeyDown(Keys.RightShift)) str = "_";
                                else str = "-";
                                break;

                            case Keys.Subtract:
                                str = "-";
                                break;

                            case Keys.Add:
                                str = "+";
                                break;

                            case Keys.OemPlus:
                                if (currentState.IsKeyDown(Keys.LeftShift) || currentState.IsKeyDown(Keys.RightShift))
                                    str += "?";
                                else str += "+";
                                break;

                            case Keys.D1:
                                if (currentState.IsKeyDown(Keys.LeftShift) || currentState.IsKeyDown(Keys.RightShift))
                                    str += "!";
                                else str += "1";
                                break;

                            case Keys.D2:
                                if (currentState.IsKeyDown(Keys.LeftShift) || currentState.IsKeyDown(Keys.RightShift))
                                    str += "\"";
                                else str += "2";
                                break;

                            case Keys.D3:
                                if (currentState.IsKeyDown(Keys.LeftShift) || currentState.IsKeyDown(Keys.RightShift))
                                    str += "#";
                                else str += "3";
                                break;

                            case Keys.D4:
                                if (currentState.IsKeyDown(Keys.LeftShift) || currentState.IsKeyDown(Keys.RightShift))
                                    str += "¤";
                                else str += "4";
                                break;

                            case Keys.D5:
                                if (currentState.IsKeyDown(Keys.LeftShift) || currentState.IsKeyDown(Keys.RightShift))
                                    str += "%";
                                else str += "5";
                                break;

                            case Keys.D6:
                                if (currentState.IsKeyDown(Keys.LeftShift) || currentState.IsKeyDown(Keys.RightShift))
                                    str += "&";
                                else str += "6";
                                break;

                            case Keys.D7:
                                if (currentState.IsKeyDown(Keys.LeftShift) || currentState.IsKeyDown(Keys.RightShift))
                                    str += "/";
                                else str += "7";
                                break;

                            case Keys.D8:
                                if (currentState.IsKeyDown(Keys.LeftShift) || currentState.IsKeyDown(Keys.RightShift))
                                    str += "(";
                                else str += "8";
                                break;

                            case Keys.D9:
                                if (currentState.IsKeyDown(Keys.LeftShift) || currentState.IsKeyDown(Keys.RightShift))
                                    str += ")";
                                else str += "9";
                                break;

                            case Keys.D0:
                                if (currentState.IsKeyDown(Keys.LeftShift) || currentState.IsKeyDown(Keys.RightShift))
                                    str += "=";
                                else str += "0";
                                break;

                            case Keys.OemCloseBrackets:
                                str += "å";
                                break;

                            case Keys.OemQuotes:
                                str += "ä";
                                break;

                            case Keys.OemTilde:
                                str += "ö";
                                break;

                            case Keys.Up:
                                if (selected_history > 0)
                                {
                                    selected_history--;
                                    input = history[selected_history];
                                }
                                break;

                            case Keys.Down:
                                if (selected_history < history.Count)
                                {
                                    selected_history++;

                                    if (selected_history < history.Count)
                                    {
                                        input = history[selected_history];
                                    }

                                    else if (selected_history == history.Count)
                                        input = "";
                                }
                                break;

                            case Keys.Tab:
                                if (string.IsNullOrEmpty(auto_fill) == false)
                                {
                                    input += auto_fill;
                                }
                                break;

                            case Keys.Enter:
                                if (string.IsNullOrEmpty(input) == false)
                                {
                                    if (history.Count == 0 || history[history.Count - 1] != input)
                                    {
                                        history.Add(input);
                                    }
                                    selected_history = history.Count;
                                    PerformCommand();
                                }
                                break;

                            default:
                                break;
                        }
                    }

                    if (uppercase) str = str.ToUpper();
                    else str = str.ToLower();

                    input += str;
                }
            }
        }

        private static void PerformCommand(string indata = "")
        {
            if (string.IsNullOrEmpty(indata) == false)
            {
                input = indata;
            }

            foreach (var keyword in keyWords)
            {
                input = input.Replace(keyword.Key, keyword.Value);
            }

            var command = input.Split(' ')[0];
            var arguments = input.Split(' ').ToList();
            arguments.RemoveAt(0);

            foreach (var item in arguments.ToList())
            {
                if (string.IsNullOrEmpty(item))
                {
                    arguments.Remove(item);
                }
            }

            input = "";

            if (string.IsNullOrWhiteSpace(command) == false)
            {
                try
                {
                    switch (command.Split('.')[0].ToLower())
                    {
                        case "debug":
                            if (game.IsDebug)
                            {
                                switch (command.ToLower())
                                {
                                    case "debug.history":
                                        PrintLine();
                                        int count = 0;
                                        history.ForEach(arg => PrintLine((++count) + ": " + arg));
                                        break;

                                    default:
                                        PrintLine("Unknown debug command \"" + command.Remove(0, "debug".Length).TrimStart('.') + "\"");
                                        break;
                                }
                            }
                            else
                            {
                                PrintLine("Game is not running a debug build.");
                            }
                            break;

                        case "version":
                            string text = "Running version \"" + game.Version + "\"";

                            if (game.IsDebug) text += " - Debug Build";

                            output.Add(text);
                            output_color.Add(Color.White);
                            break;

                        case "print":
                            if (arguments.Count < 1)
                            {
                                PrintLine("Invalid amount of arguments.");
                                break;
                            }
                            string message = "";
                            arguments.ForEach(arg => message += arg + " ");
                            PrintLine(message);
                            break;

                        case "help":
                            output.Add("");
                            output_color.Add(Color.White);
                            foreach (var item in commands)
                            {
                                output.Add(item.Key + " - " + item.Value);
                                output_color.Add(Color.White);
                            }
                            break;

                        case "clear":
                            scroll = 0;
                            output.Clear();
                            output_color.Clear();
                            break;

                        case "reset":
                            scroll = 0;
                            PerformCommand("clear");
                            PrintLine("Press § to hide/show console.");
                            PrintLine("Type \"help\" for a list of commands.");
                            PerformCommand("version");
                            PrintLine();
                            break;

                        case "scene":
                            switch (command.ToLower())
                            {
                                case "scene.select":
                                    if (arguments.Count > 0)
                                    {
                                        var obj = SceneManager.GetLoadedScene().GetObjectByName(arguments[0]);

                                        if (obj == null) PrintLine(string.Format("\"{0}\" does not exist.", obj.Name));
                                        else
                                        {
                                            keyWords["!ROOT"] = obj.Name;
                                            PrintLine(string.Format("Selected \"{0}\"", obj.Name));
                                        }
                                    }
                                    break;

                                case "scene.listobjects":
                                    var list = SceneManager.GetLoadedScene().SceneObjects.ToList();

                                    if (arguments.Count > 0)
                                    {
                                        foreach (var item in list.ToList())
                                        {
                                            if (item.Name.ToLower().Contains(arguments[0].ToLower()) == false)
                                            {
                                                list.Remove(item);
                                            }
                                        }
                                    }

                                    foreach (var item in list)
                                    {
                                        PrintLine(item.Name);
                                    }
                                    break;

                                default:
                                    PrintLine("Scene does not have such command.");
                                    break;
                            }
                            break;

                        case "scenemanager":
                            switch (command.ToLower())
                            {
                                case "scenemanager.loadscene":
                                    if (arguments.Count > 0)
                                    {
                                        SceneManager.LoadScene(arguments[0]);
                                    }
                                    break;

                                case "scenemanager.listscenes":
                                    foreach (var item in SceneManager.AllScenes())
                                    {
                                        PrintLine(item);
                                    }
                                    break;

                                default:
                                    PrintLine("SceneManager does not have such command.");
                                    break;
                            }
                            break;

                        case "exit":
                            PerformCommand("reset");
                            Hide();
                            break;

                        case "quitgame":
                            game.Exit();
                            break;

                        default:
                            output.Add("Unknown command \"" + command + "\"");
                            output_color.Add(Color.White);
                            break;
                    }
                }
                catch (Exception e)
                {
                    PrintLine(e.Message);
                }
            }
        }

        public static void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (IsVisible)
            {
                var background = TextureHandler.Rectangle((int)size.X, (int)size.Y, new Color(0, 0, 0, 220));
                var inputBackground = TextureHandler.Rectangle((int)size.X, 30, new Color(0, 0, 0, 100));
                var drawPos = Vector2.Zero;
                float textHeight = 0;

                spriteBatch.Draw(background, position, null, Color.White, 0, Vector2.Zero, 1, SpriteEffects.None, 0.89f);
                spriteBatch.Draw(inputBackground, position + new Vector2(0, size.Y - 30), null, Color.White, 0, Vector2.Zero, 1, SpriteEffects.None, 0.89f);

                for (int i = output.Count - 1, item = 0; i >= 0; i--, item++)
                {
                    drawPos = new Vector2(5, size.Y - TextHandler.Default.MeasureString(" ").Y * 2.5f - 5 + scroll);

                    drawPos.Y -= TextHandler.Default.MeasureString(" ").Y * item;
                    textHeight += TextHandler.Default.MeasureString(" ").Y;

                    if (position.Y + drawPos.Y >= position.Y &&
                        position.Y + drawPos.Y <= size.Y - inputBackground.Height)
                    {
                        spriteBatch.DrawString(TextHandler.Default, output[i], position + drawPos, output_color[i], 0, Vector2.Zero, 1, SpriteEffects.None, 0.90f);
                    }
                }

                drawPos = new Vector2(5, size.Y - TextHandler.Default.MeasureString(" ").Y - 5);

                spriteBatch.DrawString(TextHandler.Default, input, position + drawPos, Color.White, 0, Vector2.Zero, 1, SpriteEffects.None, 0.90f);

                spriteBatch.DrawString(TextHandler.Default, auto_fill, position + drawPos + new Vector2(TextHandler.Default.MeasureString(input).X, 0), Color.Gray, 0, Vector2.Zero, 1, SpriteEffects.None, 0.90f);

                if (indicator && selected)
                {
                    drawPos.X += TextHandler.Default.MeasureString(input).X;

                    spriteBatch.DrawString(TextHandler.Default, "|", position + drawPos, Color.White, 0, Vector2.Zero, 1, SpriteEffects.None, 0.90f);
                }

                var slider = TextureHandler.Rectangle(10,(int)(size.Y - position.Y), new Color(66, 66, 66, 255));
                var x = size.Y / textHeight;
                var scale = new Vector2(1, 1);
                var sliderPos = position + new Vector2(size.X - slider.Width, size.Y - inputBackground.Height - slider.Height);
                if (x < 1)
                {
                    scale.Y = x;
                    sliderPos.Y += slider.Height - slider.Height * x;

                    if (selected)
                    {
                        if (currentMouse.ScrollWheelValue - previousMouse.ScrollWheelValue > 0)
                        {
                            scroll += (int)(1 / (scale.Y / 10));
                        }
                        else if (currentMouse.ScrollWheelValue - previousMouse.ScrollWheelValue < 0)
                        {
                            scroll -= (int)(1 / (scale.Y / 10));
                        }
                    }

                    if (scroll < 0) scroll = 0;
                    while (sliderPos.Y - scroll * scale.Y < position.Y)
                        scroll--;

                    sliderPos.Y -= scroll * scale.Y;
                }

                spriteBatch.Draw(slider, sliderPos, null, Color.White, 0, Vector2.Zero, scale, SpriteEffects.None, 0.90f);

                console.Draw(gameTime, spriteBatch);
            }
        }
    }
}
