﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;

namespace MonoGame.src.Graphics
{
    public static class ScreenHandler
    {
        public enum ScreenSettings { Windowed, Fullscreen, Borderless }

        #region Settings
        static Game1 _game;
        static GraphicsDeviceManager _graphics;

        static int _screenWidth;
        static int _screenHeight;

        static ScreenSettings _screenSetting;
        #endregion

        #region Properties
        public static float Width { get => _screenWidth; }
        public static float Height { get => _screenHeight; }
        public static Point Size { get => new Point(_screenWidth, _screenHeight); }
        #endregion

        internal static void Initialize(Game1 game, ref GraphicsDeviceManager graphics,
            Point? screen_size, ScreenSettings screenSetting)
        {
            _game = game;
            _graphics = graphics;

            Point screen = new Point(640, 360);

            if (screen_size.HasValue)
                screen = screen_size.Value;

            if (screen_size.HasValue)
            {
                _screenWidth = screen.X;
                _screenHeight = screen.Y;
            }

            _screenSetting = screenSetting;

            Apply();
        }

        public static void Apply()
        {
            _graphics.PreferredBackBufferWidth = _screenWidth;
            _graphics.PreferredBackBufferHeight = _screenHeight;

            if (_screenSetting == ScreenSettings.Borderless)
            {
                _graphics.IsFullScreen = false;
                _game.Window.IsBorderless = true;
            }

            else if (_screenSetting == ScreenSettings.Fullscreen)
            {
                _graphics.IsFullScreen = false;
                _game.Window.IsBorderless = false;
            }

            else
            {
                _graphics.IsFullScreen = false;
                _game.Window.IsBorderless = false;
            }

            //V-Sync (60 FPS)
            _game.IsFixedTimeStep = true;

            _graphics.ApplyChanges();

            if (_screenSetting != ScreenSettings.Windowed)
                _game.Window.Position = new Point(0);

            _game.Window.Title = "GameFramework by 'valrossenOliver' : " + _game.Window.Title;
        }

        public static void Popup(SpriteBatch spriteBatch, string title = "title", string message = "message")
        {
            List<string> words = new List<string>();
            Vector2 size = new Vector2(400, 60);

            string currentWord = "";

            while (message.Length >= 0)
            {
                if (message.Length == 0)
                {
                    words.Add(currentWord);
                    break;
                }

                if (message[0] == '\n')
                {
                    words.Add(currentWord);
                    currentWord = "";

                    currentWord += message[0];
                    message = message.Remove(0, 1);

                    words.Add(currentWord);
                    currentWord = "";
                }
                else if (message[0] == ' ' || message[0] == ',' || message[0] == '.' || message[0] == '!' || message[0] == '?' || message[0] == '-')
                {
                    currentWord += message[0];
                    message = message.Remove(0, 1);

                    words.Add(currentWord);
                    currentWord = "";
                }
                else if (message.Length >= 2)
                {
                    currentWord += message[0];
                    message = message.Remove(0, 1);
                }
                else if (message.Length == 0)
                {
                    words.Add(currentWord);
                    currentWord = "";
                }
                else
                {
                    currentWord += message[0];
                    message = message.Remove(0, 1);
                }
            }

            float total_width = 0;
            float total_height = 0;

            string line = "";
            int count = 0;

            foreach (var word in words)
            {
                count++;
                var linewidth = TextHandler.Default.MeasureString(line).X;
                var wordwidth = TextHandler.Default.MeasureString(word).X;

                var total = TextHandler.Default.MeasureString(string.Concat(line, word)).X;
                if (TextHandler.Default.MeasureString(string.Concat(line, word)).X <= size.X && word != "\n")
                {
                    line += word;
                }
                else
                {
                    if (TextHandler.Default.MeasureString(line).X > total_width)
                    {
                        total_width = TextHandler.Default.MeasureString(line).X;
                    }

                    total_height += TextHandler.Default.MeasureString(line).Y;

                    line = "";

                    if (word != "\n")
                        line += word;
                }

                if (count == words.Count)
                {
                    if (TextHandler.Default.MeasureString(line).X > total_width)
                    {
                        total_width = TextHandler.Default.MeasureString(line).X;
                    }

                    total_height += TextHandler.Default.MeasureString(line).Y;

                    line = "";
                }
            }

            size.Y += total_height;
            size.Y += TextHandler.Default.MeasureString(title).Y;

            var background = TextureHandler.Rectangle((int)size.X, (int)size.Y, Color.LightGray);
            var titleBar = TextureHandler.Rectangle((int)size.X, 30, Color.DarkGray);

            spriteBatch.Draw(background, new Vector2(Size.X / 2 - size.X / 2, Size.Y / 2 - size.Y / 2), null, Color.White, 0,
                Vector2.Zero, 1, SpriteEffects.None, 0.98f);

            spriteBatch.Draw(titleBar, new Vector2(Size.X / 2 - size.X / 2, Size.Y / 2 - size.Y / 2), null, Color.White, 0,
                Vector2.Zero, 1, SpriteEffects.None, 0.99f);

            spriteBatch.DrawString(TextHandler.Default, title,
                        new Vector2(Size.X / 2, Size.Y / 2 - size.Y / 2 + 5), Color.Black, 0,
                        new Vector2(TextHandler.Default.MeasureString(title).X / 2, 0), 1, SpriteEffects.None, 1f);

            Vector2 Offset = Vector2.Zero;

            //Text
            for (int i = 0; i < words.Count; i++)
            {
                Vector2 word_size = TextHandler.Default.MeasureString(words[i]);
                currentWord = words[i]; //DEBUG PURPOSES

                if (words[i] == "\n")
                {
                    Offset.Y += TextHandler.Default.MeasureString(" ").Y;
                    Offset.X = 0;
                }
                else
                {
                    if (word_size.X + Offset.X > size.X)
                    {
                        Offset.X = 0;
                        Offset.Y += word_size.Y;
                    }

                    spriteBatch.DrawString(TextHandler.Default, words[i],
                        new Vector2(Size.X / 2 - size.X / 2 + 5, Size.Y / 2 - size.Y / 2 + 50) + Offset, Color.Black, 0,
                        Vector2.Zero, 1, SpriteEffects.None, 1f);

                    Offset.X += word_size.X;
                }
            }
        }
    }
}
