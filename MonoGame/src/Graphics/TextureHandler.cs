﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace MonoGame.src.Graphics
{
    public static class TextureHandler
    {
        static ContentManager Content;
        static GraphicsDevice Graphics;

        static Texture2D _pixel;

        public enum DrawMode { Fill, Border }

        internal static void Initialize(ContentManager content, GraphicsDevice graphics)
        {
            Content = content;
            Graphics = graphics;

            _pixel = new Texture2D(graphics, 1, 1);
            Color[] data = { Color.White };
            _pixel.SetData(data);
        }

        /// <summary>
        /// Returns a 1x1 white pixel
        /// </summary>
        public static Texture2D Pixel { get => _pixel; }

        /// <summary>
        /// Creates a new texture of specified width, height and color.
        /// </summary>
        /// <param name="w">Width</param>
        /// <param name="h">Height</param>
        /// <param name="color">Color</param>
        /// <returns></returns>
        public static Texture2D Rectangle(int w, int h, Color color, DrawMode mode = DrawMode.Fill)
        {
            Texture2D texture = new Texture2D(Graphics, w, h);
            Color[] data = new Color[w * h];

            for (int y = 0; y < h; y++)
            {
                for (int x = 0; x < w; x++)
                {
                    if (mode == DrawMode.Fill)
                        data[y * w + x] = color;

                    else if (mode == DrawMode.Border)
                    {
                        if (y == 0 || y == h - 1 || x == 0 || x == w - 1)
                        {
                            data[y * w + x] = color;
                        }
                        else
                        {
                            data[y * w + x] = Color.Transparent;
                        }
                    }
                }
            }

            texture.SetData(data);

            return texture;
        }

        /// <summary>
        /// Creates a round cirle of specified diameter and color.
        /// </summary>
        /// <param name="d"></param>
        /// <param name="color"></param>
        /// <returns></returns>
        public static Texture2D Circle(int d, Color color, DrawMode mode = DrawMode.Fill)
        {
            d++;

            Texture2D texture = new Texture2D(Graphics, d, d);
            Color[] data = new Color[d * d];

            for (int y = 0; y < d; y++)
            {
                for (int x = 0; x < d; x++)
                {
                    //Variables
                    Vector2 Origo = new Vector2(d, d) / 2; //Middle of circle
                    Vector2 Position = new Vector2(x, y); //Position to check

                    //Get a vector representing the distance between point and origo
                    Vector2 vector = Vector2.Subtract(Position, Origo);

                    //Use Pythagoras theorem to get the distance
                    double distance = Math.Sqrt(Math.Pow(vector.X, 2) + Math.Pow(vector.Y, 2));

                    //If distance is less or equal to the radius

                    if (mode == DrawMode.Fill && distance <= d / 2)
                    {
                        data[y * d + x] = color;
                    }
                    else if (mode == DrawMode.Border && Math.Round(distance) == d / 2)
                    {
                        data[y * d + x] = color;
                    }
                    else data[y * d + x] = Color.Transparent;
                }
            }

            texture.SetData(data);

            return texture;
        }

        public static Texture2D Load(string asset)
        {
            var texture = Content.Load<Texture2D>("Images/" + asset);

            return texture;
        }
    }
}
