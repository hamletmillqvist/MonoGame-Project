﻿using Microsoft.Xna.Framework;
using MonoGame.src.Components;
using System;

namespace MonoGame.src
{
    public class Transform : Component
    {
        #region Fields

        private Vector2 position;
        private Vector2 size;
        private float scale;
        private float rotation;

        #endregion

        #region Properties

        public Vector2 Position { get => position; set => position = value; }
        public Vector2 Size { get => size; set => size = value; }
        public float Scale { get => scale; set => scale = value; }
        public float Rotation { get => rotation; set => rotation = value; }

        public Transform Parent
        {
            get
            {
                if (gameObject != null && gameObject.Parent != null)
                {
                    return gameObject.Parent.transform;
                }
                else return null;
            }
            set
            {
                if (gameObject.Parent != null)
                    gameObject.Parent.Children.Remove(gameObject);

                gameObject.Parent = value.gameObject;
            }
        }
        public Vector2 localPosition
        {
            get
            {
                if (Parent != null)
                {
                    return Position - Parent.Position;
                }
                else
                {
                    return Position;
                }
            }
            set
            {
                if (Parent != null)
                {
                    Position = Parent.Position + value;
                }
                else
                {
                    Position = value;
                }
            }
        }
        public Vector2 localSize
        {
            get
            {
                if (Parent != null)
                {
                    return Size / Parent.Size;
                }
                else
                {
                    return Size;
                }
            }
            set
            {
                if (Parent != null)
                {
                    Size = value * Parent.Size;
                }
                else
                {
                    Size = value;
                }
            }
        }
        public float localScale
        {
            get
            {
                if (Parent != null)
                {
                    return Scale / Parent.Scale;
                }
                else
                {
                    return Scale;
                }
            }
            set
            {
                if (Parent != null)
                {
                    Scale = value * Parent.Scale;
                }
                else
                {
                    Scale = value;
                }
            }
        }
        public float localRotation
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        #endregion

        #region Constuctors

        public Transform()
        {
            position = Vector2.Zero;
            size = Vector2.One;
            scale = 1;
            Rotation = 0;
        }

        public Transform(float x, float y, float width = 1, float height = 1)
        {
            position = new Vector2(x, y);
            size = new Vector2(width, height);
            scale = 1;
            rotation = 0;
        }

        public Transform(Vector2 pos, Vector2? siz = null)
        {
            position = pos;
            size = siz ?? Vector2.One;
            scale = 1;
            rotation = 0;
        } 

        #endregion

        #region Math

        public static Transform Add(Transform _source, Transform _other)
        {
            Transform _transform = new Transform
            {
                Position = _source.Position + _other.Position,
                Size = _source.Size + _other.Size,
                Scale = _source.Scale + _other.Scale,
            };

            return _transform;
        }

        public static Transform Subtract(Transform _source, Transform _other)
        {
            Transform _transform = new Transform
            {
                Position = _source.Position - _other.Position,
                Size = _source.Size - _other.Size,
                Scale = _source.Scale - _other.Scale,
            };

            return _transform;
        }

        #endregion

        #region Operators

        public static Transform operator+ (Transform _left, Transform _right)
        {
            return Transform.Add(_left, _right);
        }

        public static Transform operator- (Transform _left, Transform _right)
        {
            return Transform.Subtract(_left, _right);
        }

        #endregion

        #region Methods

        public Vector2 RelativePositionTo (GameObject _gameObject)
        {
            return RelativePositionTo(_gameObject.transform.Position);
        }

        public Vector2 RelativePositionTo(Vector2 _vector)
        {
            return Vector2.Subtract(Position, _vector);
        }

        public override string ToString()
        {
            return "{" + "X:" + Position.X + " Y:" + Position.Y +
                " W:" + (Size.X * Scale) + " H:" + (Size.Y * Scale) + "}";
        }

        #endregion

        #region GameLoop

        public override void Update(GameTime gameTime)
        {
        }

        public override void PostUpdate(GameTime gameTime)
        {
        } 

        #endregion
    }
}
