﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.src.Components;
using System.Collections.Generic;

namespace MonoGame.src
{
    public class GameObject
    {
        #region Fields

        private string name;
        private string tag;
        private bool enabled;
        private bool destroyed;

        private GameObject parent;
        private Transform transfrm;
        private List<GameObject> children;
        private List<Component> components;

        private float layerDepth;

        public enum Templates { Button }

        #endregion

        #region Properties

        public string Name { get => name; }
        public string Tag { get => tag; set => tag = value; }
        public bool Enabled { get => enabled; }
        public bool Destroyed { get => destroyed; }

        public ref GameObject Parent { get => ref parent; }
        public List<GameObject> Children { get => children; set => children = value; }
        public List<Component> Components { get => components; set => components = value; }
        public ref Transform transform { get => ref transfrm; }

        public float LayerDepth { get => layerDepth; set => layerDepth = value; }

        #endregion

        public delegate void GameObjectHandler(GameObject sender);
        public static event GameObjectHandler OnObjectCreated;

        /// <summary>
        /// Creates a new container (GameObject) for components.
        /// </summary>
        /// <param name="_name"></param>
        /// <param name="_tag"></param>
        protected GameObject(string _name, string _tag = "", GameObject _parent = null)
        {
            if (_name.Contains("%"))
            {
                var split = _name.Split('%');
                _name = "";

                foreach (var item in split)
                {
                    _name += item;
                }
            }

            name = _name;
            tag = _tag;
            enabled = true;
            destroyed = false;

            components = new List<Component>();
            children = new List<GameObject>();

            if (_parent != null)
            {
                Parent = _parent;
            }
            else
            {
                parent = null;
            }

            transfrm = AddComponent<Transform>() as Transform;

            layerDepth = 0;

            OnObjectCreated?.Invoke(this);
        }

        public static GameObject CreateNew(string _name,
            Vector2? pos = null, Vector2? siz = null, GameObject _parent = null)
        {
            GameObject gameObject = new GameObject(_name, "", _parent);

            gameObject.transform.Position = pos ?? Vector2.Zero;
            gameObject.transform.Size = siz ?? new Vector2(10);

            return gameObject;
        }

        public static GameObject CreateTemplate(string _name, Templates template,
            Vector2? pos = null, Vector2? siz = null, GameObject _parent = null)
        {
            var gameObject = GameObject.CreateNew(_name);

            if (_parent != null) gameObject.Parent = _parent;

            switch (template)
            {
                case Templates.Button:
                    gameObject.transform.Position = pos ?? Vector2.Zero;
                    gameObject.transform.Size = siz ?? new Vector2(200, 60);

                    gameObject.AddComponent<BoxCollider>();
                    gameObject.AddComponent<ToolTip>();
                    gameObject.AddComponent<Button>();
                    gameObject.AddComponent<Text2D>();

                    return gameObject;

                default:
                    throw new System.Exception("UNKNOWN TEMPLATE");
            }
        }

        #region Components

        /// <summary>
        /// Adds a new component to this gameobject.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public T AddComponent<T>() where T : Component, new()
        {
            var component = new T();
            component.Attach(this);
            components.Add(component);

            return component;
        }

        /// <summary>
        /// Returns true/false depending if a component of specified type is appended to the gameobject.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public bool HasComponent<T>() where T : Component
        {
            foreach (var component in components)
            {
                if (component is T)
                    return true;
            }

            return false;
        }

        /// <summary>
        /// Returns first component of the specified type.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public T GetComponent<T>() where T : Component
        {
            foreach (var component in components)
            {
                if (component is T)
                    return (T)component;
            }

            return null;
        }

        /// <summary>
        /// Returns a list of all components of the specified type.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public List<T> GetComponents<T>() where T : Component
        {
            List<T> componentList = new List<T>();

            foreach (var component in components)
            {
                if (component is T)
                    componentList.Add(component as T);
            }

            return componentList;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Sets enabled state for the GameObject. Disabled objects doesn't update or draw.
        /// </summary>
        /// <param name="value"></param>
        public void SetActive(bool value) => enabled = value;

        public void MakeChild (GameObject child)
        {
            if (child.Parent != this)
            {
                if (child.Parent != null)
                {
                    child.Parent.Children.Remove(child);
                }

                child.Parent = this;
                Children.Add(child);
            }
        }

        /// <summary>
        /// This will allow the scene to remove this object from the object list during
        /// the post update.
        /// </summary>
        public void Destroy()
        {
            destroyed = true;
        }

        /// <summary>
        /// Moves the GameObject a designated vector of size.
        /// </summary>
        /// <param name="vector">How much to move the GameObject.</param>
        /// <param name="isTimeBased">Weither the object moves in GameTime or not.</param>
        /// <param name="isStatic">Weither child-objects should follow suit or not.</param>
        public void Move(Vector2 vector, bool isTimeBased = true, bool isStatic = false)
        {
            if (isTimeBased)
            {
                //TODO : Add Timehandler
                //vector *= TimeHandler.TimeScale;
            }

            transform.Position += vector;

            if (isStatic)
            {
                foreach (var child in Children)
                {
                    child.transform.Position -= vector;
                }
            }
        }

        #endregion

        #region GameLoop

        public virtual void Update(GameTime gameTime)
        {
            foreach (var component in components)
            {
                if (component.Enabled)
                {
                    component.Update(gameTime);
                }
            }
        }

        public virtual void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            foreach (var component in components)
            {
                if (component.Enabled && component is IDrawableComponent)
                {
                    (component as IDrawableComponent).Draw(gameTime, spriteBatch);
                }
            }
        }

        public virtual void PostUpdate(GameTime gameTime)
        {
            foreach (var component in components)
            {
                component.PostUpdate(gameTime);
            }
        } 

        #endregion
    }
}