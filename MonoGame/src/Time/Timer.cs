﻿using Microsoft.Xna.Framework;
using System;
using MonoGame.src.Components;

namespace MonoGame.src.Time
{
    public class Timer : GameBehaviour
    {
        TimeSpan _delayTime;
        TimeSpan _previousCallTime;

        TimeSpan _lastGameTime;

        bool _running = false;
        int _ticks;

        public delegate void TimerHandler(GameObject sender, Timer timer);
        public event TimerHandler OnTick;

        public float Time
        {
            get => (float)_delayTime.TotalSeconds;
            set => _delayTime = TimeSpan.FromSeconds(value);
        }

        public int Ticks { get => _ticks; }

        public TimeSpan CountDown
        {
            get
            {
                TimeSpan ret_Value = _previousCallTime - _lastGameTime;
                if (_running) { ret_Value += _delayTime; }

                return ret_Value;
            }
        }

        /// <summary>
        /// This creates a delay timer for an amount of seconds.
        /// </summary>
        /// <param name="seconds"></param>
        public Timer()
        {
            _delayTime = TimeSpan.FromSeconds(1);
            _previousCallTime = TimeSpan.Zero;
            _lastGameTime = TimeSpan.Zero;

            _ticks = 0;
            _running = false;
        }

        public void Start(float time = -1)
        {
            if (time != -1)
                _delayTime = TimeSpan.FromSeconds(time);

            _running = true;
        }

        public void Reset()
        {
            _ticks = 0;
        }

        public void Stop()
        {
            _running = false;
        }

        /// <summary>
        /// This checks if the timer has reached its requested time.
        /// </summary>
        /// <param name="gameTime"></param>
        /// <returns></returns>
        private bool Done()
        {
            if (_running && _lastGameTime - _previousCallTime > _delayTime)
            {
                _previousCallTime = _lastGameTime;
                return true;
            }

            return false;
        }

        /// <summary>
        /// This is called when the timer has reached its requested time.
        /// </summary>
        private void Tick()
        {
            _ticks++;

            OnTick?.Invoke(gameObject, this);
        }

        public override void Update(GameTime gameTime)
        {
            _lastGameTime = gameTime.TotalGameTime;

            if (_running)
            {
                if (Done())
                {
                    Tick();
                }
            }
            else
            {
                _previousCallTime = _lastGameTime;
            }

            base.Update(gameTime);
        }

        public override void PostUpdate(GameTime gameTime)
        {
            
        }
    }
}
