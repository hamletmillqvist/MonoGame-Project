﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;

namespace MonoGame.src.Time
{
    public class Animation
    {
        List<Transform> _frames;
        float currentFrame;
        float totalFrames;

        public bool Running { get; private set; }
        public bool Paused { get; private set; }
        public bool Loop { get; set; }
        public List<KeyFrame> KeyFrames { get; set; }
        public float Length
        {
            get
            {
                float length = 0;

                foreach (var keyFrame in KeyFrames)
                {
                    length += keyFrame.TimeSpan;
                }

                return length;
            }
        }

        public delegate void FrameHandler(Transform transform);
        public event FrameHandler OnFrame;

        public event EventHandler AnimationStart;
        public event EventHandler AnimationEnd;

        public Animation()
        {
            KeyFrames = new List<KeyFrame>();
            Running = false;
            Paused = false;
            Loop = false;

            AddKeyFrame(0, new Transform());
        }

        /// <summary>
        /// Creates a new keyframe for the animation.
        /// </summary>
        /// <param name="time">Time for change to happen from last keyframe.</param>
        /// <param name="transform">Transform (CHANGE)</param>
        public void AddKeyFrame(float time, Transform transform)
        {
            KeyFrames.Add(new KeyFrame(time, transform));
        }

        /// <summary>
        /// Starts the animation.
        /// </summary>
        public void Play()
        {
            Running = true;

            _frames = new List<Transform>();

            totalFrames = (int)Length * 60;
            currentFrame = 0;

            //Foreach keyframe
            for (int i = 1; i < KeyFrames.Count; i++)
            {
                float movementPerUpdate =
                    (KeyFrames[i].Transform.Position.Length() / KeyFrames[i].TimeSpan) / 60;

                float frames = KeyFrames[i].TimeSpan * 60;

                for (int j = 0; j < frames; j++)
                {
                    Transform transform = new Transform
                    {
                        Position = KeyFrames[i].Transform.Position / frames,
                    };

                    _frames.Add(transform);
                }
            }

            //Broadcast that animation has started
            AnimationStart?.Invoke(this, new EventArgs());
        }

        /// <summary>
        /// Stops the animation.
        /// </summary>
        public void Stop()
        {
            Running = false;

            currentFrame = 0;
            AnimationEnd?.Invoke(this, new EventArgs());
        }

        public void Pause()
        {
            Paused = true;
        }

        public void Resume()
        {
            Paused = false;
        }

        /// <summary>
        /// Performs an update sequence
        /// </summary>
        /// <param name="gameTime"></param>
        public void Update(GameTime gameTime)
        {
            if (Running && !Paused)
            {
                if (currentFrame >= totalFrames)
                {
                    if (Loop)
                    {
                        currentFrame = 0;
                    }
                    else
                    {
                        Running = false;
                        AnimationEnd?.Invoke(this, new EventArgs());
                        return;
                    }
                }

                OnFrame?.Invoke(_frames[(int)currentFrame]);

                currentFrame += TimeHandler.TimeScale;
            }
        }
    }
}
