﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.src.Graphics;

namespace MonoGame.src.Components
{
    class Sprite : Component, IDrawableComponent
    {
        #region Fields

        private Texture2D texture;
        private Color color;
        private Vector2 position;
        private Vector2 scale;

        private bool visible;

        #endregion

        #region Properties

        public Texture2D Texture { get => texture; set => texture = value; }
        public Color Color { get => color; set => color = value; }
        public Vector2 Position { get => position; set => position = value; }
        public Vector2 Scale { get => scale; set => scale = value; }

        public bool Visible { get => visible; set => visible = value; }

        #endregion

        public Sprite()
        {
            Texture = TextureHandler.Rectangle(10, 10, Color.Gray);
            Color = Color.White;
            Position = Vector2.Zero;
            Scale = Vector2.One;

            Visible = true;
        }

        #region Methods

        #endregion

        #region GameLoop

        public override void Update(GameTime gameTime)
        {

        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (visible && texture != null)
            {
                spriteBatch.Draw(Texture, gameObject.transform.Position + Position, null, color, 0, Vector2.Zero, Scale, SpriteEffects.None, gameObject.LayerDepth);
            }
        }

        public override void PostUpdate(GameTime gameTime)
        {

        } 

        #endregion
    }
}
