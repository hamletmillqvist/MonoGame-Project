﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.src.Graphics;
using System.Collections.Generic;

namespace MonoGame.src.Components
{
    public class Text2D : Component, IDrawableComponent
    {
        public enum eAlignment { Left, Center, Right }
        public enum eDisplay { Top, Middle, Bottom }

        eAlignment _alignment;
        eDisplay _display;

        string _text;
        Vector2 _scale;
        Color _color;
        Vector2 _position;
        Vector2 _size;
        Vector2 _origin;
        SpriteFont _font;

        public Text2D()
        {
            _text = "";
            _scale = Vector2.One;
            _color = Color.Black;
            _font = TextHandler.Default;
            _position = Vector2.Zero;
            _size = new Vector2(150, 50);

            _alignment = eAlignment.Left;
            _display = eDisplay.Top;
        }

        protected override void OnAttach(Component sender)
        {
            Position = Vector2.Zero;
            Size = gameObject.transform.Size;

            Text = "lbl_" + gameObject.Name;
            Color = Color.Black;

            Align = eAlignment.Center;
            Display = eDisplay.Middle;
        }

        public string Text { get => _text; set => _text = value; }
        public Vector2 Scale { get => _scale; set => _scale = value; }
        public Color Color { get => _color; set => _color = value; }
        public Vector2 Position { get => _position; set => _position = value; }
        public Vector2 Size { get => _size; set => _size = value; }
        public SpriteFont Font { get => _font; set => _font = value; }

        /// <summary>
        /// Alignments sets the text-positioning left to right.
        /// </summary>
        public eAlignment Align { get => _alignment; set => _alignment = value; }

        /// <summary>
        /// Display sets the text-positioning top to bottom.
        /// </summary>
        public eDisplay Display { get => _display; set => _display = value; }

        public override void Update(GameTime gameTime)
        {
            Vector2 text = TextHandler.Default.MeasureString(_text);

            
        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
#if false //OLD VERSION
                Vector2 drawingPosition = gameObject.transform.Position + Position;

                switch (_alignment)
                {
                    case eAlignment.Center:
                        drawingPosition.X += Size.X / 2;
                        break;

                    case eAlignment.Right:
                        drawingPosition.X += Size.X;
                        break;
                }

                switch (_display)
                {
                    case eDisplay.Middle:
                        drawingPosition.Y += Size.Y / 2;
                        break;

                    case eDisplay.Bottom:
                        drawingPosition.Y += Size.Y;
                        break;
                }

                Vector2 scale = Scale;
                var words = _text.Split(' ');

                string line = "";
                string text = "";

                for (int i = 0; i < words.Length; i++)
                {
                    if (_font.MeasureString(line + words[i].ToString()).X > Size.X)
                    {
                        text += line + '\n';
                        line = "";
                    }

                    line += words[i].ToString() + " ";

                    if (i == words.Length - 1)
                    {
                        text += line;
                    }
                }

                switch (_alignment)
                {
                    case eAlignment.Left:
                        _origin.X = 0;
                        break;

                    case eAlignment.Center:
                        _origin.X = _font.MeasureString(text).X / 2;
                        break;

                    case eAlignment.Right:
                        _origin.X = _font.MeasureString(text).X;
                        break;
                }

                switch (_display)
                {
                    case eDisplay.Top:
                        _origin.Y = 0;
                        break;

                    case eDisplay.Middle:
                        _origin.Y = _font.MeasureString(text).Y / 2;
                        break;

                    case eDisplay.Bottom:
                        _origin.Y = _font.MeasureString(text).Y;
                        break;
                }

                spriteBatch.DrawString(_font, text, drawingPosition, _color, 0, _origin, scale, SpriteEffects.None, 0.85f);
#endif

            //seperate lines
            var lines = new List<string>();
            var textSize = new Vector2();
            var text = "";

            //take each manual call
            {
                var manualLines = _text.Split('\n');

                for (int i = 0; i < manualLines.Length - 1; i++)
                {
                    lines.Add(manualLines[i]);
                    textSize.Y += _font.MeasureString(manualLines[i]).Y;

                    if (_font.MeasureString(manualLines[i]).X > textSize.X)
                    { textSize.X = _font.MeasureString(manualLines[i]).X; }
                }

                text = manualLines[manualLines.Length - 1];
            }

            //make new lines depending on length
            var words = text.Split(' ');

            string line = "";

            for (int i = 0; i < words.Length; i++)
            {
                if (_font.MeasureString(line + words[i].ToString()).X > Size.X)
                {
                    lines.Add(line);
                    textSize.Y += _font.MeasureString(line).Y;
                    if (_font.MeasureString(line).X > textSize.X)
                    { textSize.X = _font.MeasureString(line).X; }
                    line = "";
                }

                line += words[i].ToString();

                if (i != words.Length - 1)
                {
                    line += " ";
                }
                else
                {
                    lines.Add(line);
                    textSize.Y += _font.MeasureString(line).Y;
                    if (_font.MeasureString(line).X > textSize.X)
                    { textSize.X = _font.MeasureString(line).X; }
                }
            }

            //draw each line
            var offset = new Vector2();

            foreach (var str in lines)
            {
                Vector2 drawingPosition = gameObject.transform.Position + Position;
                var origin = Vector2.Zero;
                
                switch (_alignment)
                {
                    case eAlignment.Center:
                        drawingPosition.X += Size.X / 2;
                        break;

                    case eAlignment.Right:
                        drawingPosition.X += Size.X;
                        break;
                }
                
                switch (_display)
                {
                    case eDisplay.Middle:
                        drawingPosition.Y += Size.Y / 2;
                        break;

                    case eDisplay.Bottom:
                        drawingPosition.Y += Size.Y;
                        break;
                }

                switch (_alignment)
                {
                    case eAlignment.Left:
                        origin.X = 0;
                        break;

                    case eAlignment.Center:
                        origin.X = _font.MeasureString(str).X / 2;
                        break;

                    case eAlignment.Right:
                        origin.X = _font.MeasureString(str).X;
                        break;
                }

                switch (_display)
                {
                    case eDisplay.Top:
                        origin.Y = 0;
                        break;

                    case eDisplay.Middle:
                        origin.Y = textSize.Y / 2;
                        break;

                    case eDisplay.Bottom:
                        origin.Y = textSize.Y;
                        break;
                }

                spriteBatch.DrawString(_font, str, drawingPosition + offset, _color, 0, origin, Scale * gameObject.transform.Scale, SpriteEffects.None, 0.85f);

                offset.Y += _font.MeasureString(str).Y;
            }

        }

        public override void PostUpdate(GameTime gameTime)
        {
            
        }
    }
}
