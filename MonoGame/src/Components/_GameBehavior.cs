﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace MonoGame.src.Components
{
    public abstract class GameBehaviour : Component
    {
        bool wasHovering = false;
        bool isHovering = false;

        public virtual void Start() { }

        public override void Update(GameTime gameTime) { }

        public override void PostUpdate(GameTime gameTime)
        {
            Update_Hover(gameTime);
        }

        private void Update_Hover(GameTime gameTime)
        {
            wasHovering = isHovering;

            var collider = gameObject.GetComponent<BoxCollider>() as BoxCollider;

            if (collider != null && collider.Contains(Mouse.GetState().Position))
            {
                isHovering = true;
            }
            else
            {
                isHovering = false;
            }

            if (wasHovering == false && isHovering == true)
            { OnMouseEnter(gameTime); }

            else if (wasHovering == true && isHovering == true)
            { OnMouseOver(gameTime); }

            else if (wasHovering == true && isHovering == false)
            { OnMouseExit(gameTime); }
        }

        public virtual void OnMouseEnter(GameTime gameTime)
        {

        }

        public virtual void OnMouseOver(GameTime gameTime)
        {

        }

        public virtual void OnMouseExit(GameTime gameTime)
        {

        }
    }
}
