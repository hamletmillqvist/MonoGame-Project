﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MonoGame.src.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MonoGame.src.Components
{
    #region Condition System
    public class Condition
    {
        string name;
        string description;
        bool fullfilled;
        List<Condition> subConditions;
        float requiredSubconditions;

        public string Name { get => name; }
        public string Description { get => description; }
        public bool Fullfilled { get => fullfilled; }
        public List<Condition> SubConditions { get => subConditions; }
        public float RequiredSubconditions { get => requiredSubconditions; set => requiredSubconditions = value; }

        private float All => SubConditions.Count;

        public Condition(string name, string description = "", bool requirement = false)
        {
            this.name = name;
            this.description = description;
            this.fullfilled = requirement;

            if (string.IsNullOrEmpty(this.description))
            {
                this.description = this.name.ToLower() + "_description";
            }

            this.subConditions = new List<Condition>();
            this.RequiredSubconditions = 0;
        }

        public void AddSubcondition(string name, string description = "", bool fullfilled = false, bool requirement = true)
        {
            var condition = new Condition(name, description, fullfilled);

            condition.OnConditionChange += OnSubconditionChange;

            SubConditions.Add(condition);
            OnSubconditionChange(condition);

            if (requirement)
                RequiredSubconditions++;
        }

        public Condition GetSubCondition(string name)
        {
            foreach (var condition in SubConditions)
            {
                if (condition.Name == name) return condition;
            }

            return null;
        }

        private void OnSubconditionChange(Condition condition)
        {
            OnConditionChange?.Invoke(condition);
            Update(condition.Fullfilled);
        }

        public void Update(bool requirement)
        {
            bool changed = false;
            bool wasMet = false;

            if (RequiredSubconditions > All)
                RequiredSubconditions = All;

            if (SubConditions.Count > 0)
            {
                float conditionsMet = 0;

                foreach (var condition in SubConditions)
                {
                    if (condition.Fullfilled == true)
                    {
                        conditionsMet++;

                        if (conditionsMet >= requiredSubconditions)
                            break;
                    }
                }

                requirement = conditionsMet >= RequiredSubconditions;
            }

            if (requirement != fullfilled)
                changed = true;

            if (fullfilled == false && requirement == true)
                wasMet = true;

            fullfilled = requirement;

            if (changed)
                OnConditionChange?.Invoke(this);

            if (wasMet)
                OnConditionMet?.Invoke(this);
        }

        public delegate void ConditionHandler(Condition condition);
        public event ConditionHandler OnConditionChange;
        public event ConditionHandler OnConditionMet;
    }
    #endregion

    public class Button : Component, IDrawableComponent
    {
        #region Nested Types

        public struct TintSettings
        {
            public Color Normal;
            public Color Hover;
            public Color Pressed;
            public Color Disabled;

            public TintSettings(Color normal, Color hover,
                Color pressed, Color disabled)
            {
                Normal = normal;
                Hover = hover;
                Pressed = pressed;
                Disabled = disabled;
            }
        }
        public struct SwapSettings
        {
            public Texture2D Normal;
            public Texture2D Hover;
            public Texture2D Pressed;
            public Texture2D Disabled;

            public SwapSettings(Texture2D normal = null, Texture2D hover = null,
                Texture2D pressed = null, Texture2D disabled = null)
            {
                Normal = normal;
                Hover = hover;
                Pressed = pressed;
                Disabled = disabled;
            }
        }

        #endregion

        #region Fields

        enum ButtonEffects { None, Tint, Swap }

        ButtonEffects _buttonEffects;

        bool _visible;
        bool _interactable;
        bool _hovering;

        bool _enteredWhilePressing;
        bool _leftWhilePressing;

        MouseState _previousMouse;
        MouseState _currentMouse;

        TintSettings _tintSetting;
        SwapSettings _swapSetting;

        //Conditions needed to show the button.
        List<Condition> _potentialRequirements;

        //Conditions needed to allow using the button.
        List<Condition> _allowRequiremets;

        static TimeSpan _lastButtonCall = TimeSpan.Zero;

        #endregion

        #region Properties

        public bool IsActive { get; set; }
        public bool IsHovering { get => _hovering; }
        public bool Interactable { get => _interactable; }
        public bool Visible { get => _visible; }

        public ref TintSettings TintSetting { get => ref _tintSetting; }
        public ref SwapSettings SwapSetting { get => ref _swapSetting; }
        public List<Condition> AllRequirements
        {
            get
            {
                var requirements = _potentialRequirements.ToList();
                foreach (var requirement in _allowRequiremets)
                {
                    requirements.Add(requirement);
                }

                return requirements;
            }
        } 

        #endregion

        public Button()
        {
            _interactable = true;
            _hovering = false;
            _visible = true;
            IsActive = false;

            _enteredWhilePressing = false;
            _leftWhilePressing = false;

            _tintSetting.Normal = Color.White;
            _tintSetting.Hover = Color.LightGray;
            _tintSetting.Pressed = Color.Gray;
            _tintSetting.Disabled = Color.DarkGray;

            _swapSetting.Normal = TextureHandler.Pixel;

            _buttonEffects = ButtonEffects.Tint;

            _potentialRequirements = new List<Condition>();

            _allowRequiremets = new List<Condition>();
        }

        protected override void OnAttach(Component sender)
        {
            _swapSetting.Normal = TextureHandler.Rectangle((int)gameObject.transform.Size.X,
                (int)gameObject.transform.Size.Y, Color.White);
        }

        public event ButtonHandler OnClick;
        public delegate void ButtonHandler(GameObject gameObject);

        #region Methods

        public void SetVisibility(bool value) => _visible = value;

        public Condition GetCondition(string name)
        {
            foreach (var condition in AllRequirements)
            {
                if (condition.Name == name) return condition;
            }

            return null;
        }

        private void OnConditionsChange(Condition condition)
        {
            if (gameObject.HasComponent<ToolTip>())
            {
                (gameObject.GetComponent<ToolTip>() as ToolTip).
                    UpdateConditions(AllRequirements);
            }
        }

        /// <summary>
        /// Sets the button effects to none.
        /// </summary>
        public void SetButtonEffect()
        {
            _buttonEffects = ButtonEffects.None;
        }

        /// <summary>
        /// Sets the button effects to that of the inserted tint-settings.
        /// </summary>
        /// <param name="tintSetting"></param>
        public void SetButtonEffect(TintSettings tintSettings)
        {
            _tintSetting.Normal = tintSettings.Normal;
            _tintSetting.Hover = tintSettings.Hover;
            _tintSetting.Pressed = tintSettings.Pressed;
            _tintSetting.Disabled = tintSettings.Disabled;

            _buttonEffects = ButtonEffects.Tint;
        }

        /// <summary>
        /// Sets the button effects to that of the inserted swap-settings.
        /// </summary>
        /// <param name="tintSetting"></param>
        public void SetButtonEffect(SwapSettings swapSettings)
        {
            if (swapSettings.Normal != null)
                _swapSetting.Normal = swapSettings.Normal;

            _swapSetting.Hover = swapSettings.Hover;
            _swapSetting.Pressed = swapSettings.Pressed;
            _swapSetting.Disabled = swapSettings.Disabled;

            _buttonEffects = ButtonEffects.Swap;
        }

        /// <summary>
        /// Allows changing weither the button can be pressed or not.
        /// </summary>
        /// <param name="value"></param>
        public void SetInteractable(bool value)
        {
            _interactable = value;
        }

        /// <summary>
        /// Requirements for SHOWING the button.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="description"></param>
        /// <param name="requirement"></param>
        public void AddPotentialRequirement(string name, string description = "",
            bool requirement = false)
        {
            var condition = new Condition(name, description, requirement);

            condition.OnConditionChange += OnConditionsChange;

            _potentialRequirements.Add(condition);
            OnConditionsChange(condition);
        }

        /// <summary>
        /// Requirements for PRESSING the button.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="description"></param>
        /// <param name="requirement"></param>
        public void AddAllowRequirement(string name, string description = "",
            bool requirement = false)
        {
            var condition = new Condition(name, description, requirement);

            condition.OnConditionChange += OnConditionsChange;

            _allowRequiremets.Add(condition);
            OnConditionsChange(condition);
        }

        /// <summary>
        /// Calculates the state of this button-script, and sends an event
        /// if the button is activated. Event carries the scripts GameObject.
        /// </summary>
        private void ButtonStateCalculation(GameTime gameTime)
        {
            _previousMouse = _currentMouse;
            _currentMouse = Mouse.GetState();

            //If button is useable
            if (_interactable && gameObject.HasComponent<BoxCollider>())
            {
                //If mouse is present
                if ((gameObject.GetComponent<BoxCollider>() as BoxCollider)
                    .Contains(_currentMouse.Position))
                {
                    _hovering = true;

                    if (_hovering)
                    {
                        GameCursor.Active = true;
                    }

                    //If mouse is down
                    if (_currentMouse.LeftButton == ButtonState.Pressed)
                    {
                        if (!_leftWhilePressing
                            && _previousMouse.LeftButton == ButtonState.Pressed &&
                            ((gameObject.GetComponent<BoxCollider>() as BoxCollider).
                            Contains(_previousMouse.Position) == false ||
                            _enteredWhilePressing == true))
                        {
                            _enteredWhilePressing = true;
                        }
                        else
                        {
                            IsActive = true;
                            _enteredWhilePressing = false;
                            _leftWhilePressing = false;
                        }
                    }

                    //If mouse was just pressed (mouse released)
                    if (_previousMouse.LeftButton == ButtonState.Pressed &&
                        _currentMouse.LeftButton == ButtonState.Released &&
                        _enteredWhilePressing == false)
                    {
                        if (gameTime.TotalGameTime.TotalMilliseconds - _lastButtonCall.TotalMilliseconds > 200)
                        {
                            _lastButtonCall = gameTime.TotalGameTime;
                            IsActive = false;
                            OnClick?.Invoke(gameObject);
                        }
                    }
                }
                else
                {
                    if (IsActive && _currentMouse.LeftButton == ButtonState.Pressed)
                    {
                        _leftWhilePressing = true;
                    }
                    else if (_currentMouse.LeftButton == ButtonState.Released)
                    {
                        _leftWhilePressing = false;
                    }

                    _hovering = false;
                    IsActive = false;
                }
            }
            else
            {
                _leftWhilePressing = false;
                _enteredWhilePressing = false;
            }
        }

        #endregion

        #region GameLoop

        public override void Update(GameTime gameTime)
        {
            if (Enabled)
            {
                if (_potentialRequirements.Count > 0)
                {
                    bool potential = true;

                    foreach (var requirement in _potentialRequirements)
                    {
                        if (requirement.Fullfilled == false)
                        {
                            potential = false;
                            break;
                        }
                    }

                    SetVisibility(potential);
                }

                if (_allowRequiremets.Count > 0)
                {
                    bool allow = true;

                    foreach (var requirement in _allowRequiremets)
                    {
                        if (requirement.Fullfilled == false)
                        {
                            allow = false;
                            break;
                        }
                    }

                    SetInteractable(allow);
                }

                ButtonStateCalculation(gameTime);

                foreach (var child in gameObject.Children)
                {
                    if (child.HasComponent<Text2D>() && child.Name.StartsWith(gameObject.Name))
                    {
                        child.transform.Position = gameObject.transform.Position;
                    }
                }
            }
        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (Visible)
            {
                var color = Color.White;
                var texture = _swapSetting.Normal;

                if (_buttonEffects == ButtonEffects.Tint)
                {
                    if (_interactable)
                    {
                        if (_hovering)
                        {
                            if (Mouse.GetState().LeftButton == ButtonState.Pressed)
                                color = _tintSetting.Pressed;

                            else
                                color = _tintSetting.Hover;
                        }
                        else
                        {
                            color = _tintSetting.Normal;
                        }
                    }
                    else
                    {
                        color = _tintSetting.Disabled;
                    }
                }
                else if (_buttonEffects == ButtonEffects.Swap)
                {
                    if (_interactable)
                    {
                        if (IsActive && _swapSetting.Pressed != null)
                        {
                            texture = _swapSetting.Pressed;
                        }
                        else if (_hovering && _swapSetting.Hover != null)
                        {
                            texture = _swapSetting.Hover;
                        }
                    }
                    else if (_swapSetting.Disabled != null)
                    {
                        texture = _swapSetting.Disabled;
                    }
                }

                var transform = gameObject.transform;
                Rectangle rectangle = new Rectangle(transform.Position.ToPoint(), transform.Size.ToPoint());

                spriteBatch.Draw(texture, rectangle, null, color, 0.0f,
                    Vector2.Zero, SpriteEffects.None, 0.7f);
            }
        }

        public override void PostUpdate(GameTime gameTime)
        {
            
        } 

        #endregion
    }
}
