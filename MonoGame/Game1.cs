﻿using Microsoft.Xna.Framework;
using MonoGame.src;
using MonoGame.src.SceneManagement;
using MonoGame.Assets.Scenes;
using System;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.src.Graphics;

namespace MonoGame
{

    public class Game1 : Game
    {
        GraphicsDeviceManager _graphics;
        string _version;
        bool _debug;
        bool _exiting;

        public string Version { get => _version; }
        public bool IsDebug { get => _debug; }

        public Game1()
        {
            _graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            _version = "0.1 Alpha";
            _debug = true;
            _exiting = false;

            IsMouseVisible = false;
        }
        
        protected override void Initialize()
        {
            if (GameFramework.Init(this, ref _graphics, Content, new Point(1920, 1080),
                src.Graphics.ScreenHandler.ScreenSettings.Borderless) == false)
            {
                throw new Exception("-- FRAMEWORK COULD NOT LOAD --");
            }

            //Add all scenes
            SceneManager.AddScene(new MainMenu()); //Start Menu
            SceneManager.AddScene(new CharacterCreation()); //New Game
            SceneManager.AddScene(new Village()); //"Safe Zone"

            //Load first scene
            SceneManager.LoadScene("MainMenu");

            base.Initialize();
        }

        
        protected override void LoadContent()
        {
        }

        
        protected override void UnloadContent()
        {
        }
        
        protected override void Update(GameTime gameTime)
        {
            SceneManager.Update(gameTime);

            base.Update(gameTime);

            if (_exiting)
            {
                Exit();
            }
        }

        protected override void Draw(GameTime gameTime)
        {
            SceneManager.Draw(gameTime);

            base.Draw(gameTime);
        }

        public void Quit() => _exiting = true;
    }
}
