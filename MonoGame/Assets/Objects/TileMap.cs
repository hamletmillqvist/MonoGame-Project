﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.src;
using MonoGame.src.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoGame.Assets.Objects
{
    class TileMap : GameObject
    {
        private List<Tile> _tiles;

        public TileMap(string mapName = "TileMap", int width = 1, int height = 1, Texture2D fillTexture = null) : base(mapName)
        {
            transform.Size = new Vector2(width, height);
            transform.Position = new Vector2(0, 0);

            _tiles = new List<Tile>();

            //Generate empty tiles
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    var tile = new Tile(tileMap: this);
                    tile.transform.Position = new Vector2(60 * x, 60 * y);

                    if (fillTexture != null)
                    {
                        tile.GetComponent<Sprite>().Texture = fillTexture;
                    }

                    tile.LayerDepth = 0.1f;

                    _tiles.Add(tile);
                }
            }
        }

        /// <summary>
        /// Returns tile at requested coordinates. If none avalible, returns null.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public Tile GetTile(int x, int y)
        {
            //If within range
            if (x <= transform.Size.X && y <= transform.Size.Y)
            {
                var tileIndex = (y - 1) * (int)transform.Size.X + (x - 1);

                if (tileIndex <= _tiles.Count) return _tiles[tileIndex];
                else return null;
            }
            else return null;
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            base.Draw(gameTime, spriteBatch);

            foreach (var tile in _tiles)
            {
                tile.Draw(gameTime, spriteBatch);
            }
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            foreach (var tile in _tiles)
            {
                tile.Update(gameTime);
            }
        }

        public override void PostUpdate(GameTime gameTime)
        {
            base.PostUpdate(gameTime);

            foreach (var tile in _tiles)
            {
                tile.PostUpdate(gameTime);
            }
        }
    }
}
