﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Media;
using MonoGame.src.Components;
using MonoGame.src.Graphics;
using MonoGame.src.SceneManagement;
using MonoGame.src.Time;

namespace MonoGame.Assets.Scenes
{
    class Transision : GameBehaviour
    {
        public Scene scene;

        enum eCurrentView { Day, Night }
        eCurrentView currentView = eCurrentView.Night;

        public Color dayColor;
        public Color nightColor;
        private Vector3 currentColor;

        public Color dayClouds;
        public Color nightClouds;
        private Vector3 currentClouds;

        public Color sunColor;
        public Color moonColor;
        private Vector3 orbColor;

        float starBrightness = 1.0f;

        Timer transtionTimer;
        Timer dayNightTimer;

        Timer restartTimer;

        public Transision()
        {
            dayColor = Color.RoyalBlue;
            nightColor = new Color(0, 0, 130, 255);

            dayClouds = Color.White;
            nightClouds = Color.Gray;

            sunColor = new Color(230, 240, 110);
            moonColor = new Color(255, 0, 0);
        }

        public override void PostUpdate(GameTime gameTime)
        {
            base.PostUpdate(gameTime);
        }

        public override void Start()
        {
            transtionTimer = new Timer
            {
                Time = (1 / 100.0f)
            };

            dayNightTimer = new Timer
            {
                Time = 55
            };

            restartTimer = new Timer
            {
                Time = 5
            };

            transtionTimer.Start();
            dayNightTimer.Start();

            transtionTimer.OnTick += TranstionTimer_OnTick;
            dayNightTimer.OnTick += DayNightTimer_OnTick;
            restartTimer.OnTick += RestartTimer_OnTick;

            MediaPlayer.MediaStateChanged += MediaPlayer_MediaStateChanged;

            TranstionTimer_OnTick(null, transtionTimer);

            base.Start();
        }

        private void RestartTimer_OnTick(src.GameObject sender, Timer timer)
        {
            transtionTimer.Reset();
            dayNightTimer.Reset();

            currentView = eCurrentView.Night;
            TranstionTimer_OnTick(null, transtionTimer);

            restartTimer.Stop();
            restartTimer.Reset();

            MediaPlayer.Stop();
            scene.GetObjectByName("menu").GetComponent<AudioPlayer>().PlaySong("Lullaby");
        }

        private void MediaPlayer_MediaStateChanged(object sender, System.EventArgs e)
        {
            restartTimer.Start();
        }

        private void DayNightTimer_OnTick(src.GameObject sender, Timer timer)
        {
            if (currentView == eCurrentView.Day)
                currentView = eCurrentView.Night;

            else if (currentView == eCurrentView.Night)
                currentView = eCurrentView.Day;

            transtionTimer.Start();
        }

        private void TranstionTimer_OnTick(src.GameObject sender, Timer timer)
        {
            if (currentView == eCurrentView.Day)
            {
                //transition to night
                currentColor = dayColor.ToVector3();
                currentClouds = dayClouds.ToVector3();
                orbColor = sunColor.ToVector3();
                starBrightness = 0;

                for (int i = 0; i < transtionTimer.Ticks; i++)
                {
                    currentColor.X += ((nightColor.ToVector3().X - dayColor.ToVector3().X) / 1000f);
                    currentColor.Y += ((nightColor.ToVector3().Y - dayColor.ToVector3().Y) / 1000f);
                    currentColor.Z += ((nightColor.ToVector3().Z - dayColor.ToVector3().Z) / 1000f);

                    currentClouds.X += ((nightClouds.ToVector3().X - dayClouds.ToVector3().X) / 1000f);
                    currentClouds.Y += ((nightClouds.ToVector3().Y - dayClouds.ToVector3().Y) / 1000f);
                    currentClouds.Z += ((nightClouds.ToVector3().Z - dayClouds.ToVector3().Z) / 1000f);

                    orbColor.X += ((moonColor.ToVector3().X - sunColor.ToVector3().X) / 1000f);
                    orbColor.Y += ((moonColor.ToVector3().Y - sunColor.ToVector3().Y) / 1000f);
                    orbColor.Z += ((moonColor.ToVector3().Z - sunColor.ToVector3().Z) / 1000f);

                    starBrightness += (1 - 0) / 1000f;
                }
            }
            else
            {
                //transition to day
                currentColor = nightColor.ToVector3();
                currentClouds = nightClouds.ToVector3();
                orbColor = moonColor.ToVector3();
                starBrightness = 1;

                for (int i = 0; i < transtionTimer.Ticks; i++)
                {
                    currentColor.X += ((dayColor.ToVector3().X - nightColor.ToVector3().X) / 1000f);
                    currentColor.Y += ((dayColor.ToVector3().Y - nightColor.ToVector3().Y) / 1000f);
                    currentColor.Z += ((dayColor.ToVector3().Z - nightColor.ToVector3().Z) / 1000f);

                    currentClouds.X += ((dayClouds.ToVector3().X - nightClouds.ToVector3().X) / 1000f);
                    currentClouds.Y += ((dayClouds.ToVector3().Y - nightClouds.ToVector3().Y) / 1000f);
                    currentClouds.Z += ((dayClouds.ToVector3().Z - nightClouds.ToVector3().Z) / 1000f);

                    orbColor.X += ((sunColor.ToVector3().X - moonColor.ToVector3().X) / 1000f);
                    orbColor.Y += ((sunColor.ToVector3().Y - moonColor.ToVector3().Y) / 1000f);
                    orbColor.Z += ((sunColor.ToVector3().Z - moonColor.ToVector3().Z) / 1000f);

                    starBrightness += (0 - 1) / 1000f;
                }
            }

            if (transtionTimer.Ticks >= 100.0f * 10)
            {
                transtionTimer.Stop();
                transtionTimer.Reset();
            }
        }

        public override void Update(GameTime gameTime)
        {
            transtionTimer.Update(gameTime);
            dayNightTimer.Update(gameTime);

            scene.GetObjectsByName("cloud").ForEach(cloud => cloud.GetComponent<Sprite>().Color = new Color(currentClouds));
            scene.GetObjectByName("sun").GetComponent<Sprite>().Color = new Color(orbColor);

            foreach (var star in scene.GetObjectsByName("star"))
            {
                star.GetComponent<Sprite>().Color = new Color(new Vector3(starBrightness));

                if (starBrightness < 0.37f)
                {
                    star.GetComponent<Sprite>().Visible = false;
                }
                else
                {
                    star.GetComponent<Sprite>().Visible = true;
                }
            }

            scene.BackgroundColor = new Color(currentColor);

            base.Update(gameTime);
        }
    }
}
