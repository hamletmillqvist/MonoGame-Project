﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;
using MonoGame.src;
using MonoGame.src.Components;
using MonoGame.src.Graphics;
using MonoGame.src.SceneManagement;
using System;
using System.Collections.Generic;

namespace MonoGame.Assets.Scenes
{
    class CharacterCreation : Scene
    {
        //Character creation items
        enum Menues { Difficulty, ChildHood, AdultLife, Motivation, Confirmation }
        enum Difficulty { Male, Female, Enuch }
        enum ChildHood { Streets, Slave, Serf, Townsfolk, Clergy, Nobility }
        enum AdultLife { Beggar, Slave, Farmer, Trader, Crafter, Student, Courtier }
        enum Motivation { Justice, Hope, Religion, Revenge }

        Menues _currentMenu = Menues.Difficulty;
        Difficulty _difficulty;
        ChildHood _childHood;
        AdultLife _adultLife;
        Motivation _motivation;

        public CharacterCreation() : base("CharacterCreation")
        {
            #region Allways visible
            var background = GameObject.CreateNew("BackgroundImage");
            background.AddComponent<Sprite>().Texture = content.Load<Texture2D>("Images/paper_background");
            background.AddComponent<AudioPlayer>().AddEffect(content, "pensil");
            background.GetComponent<AudioPlayer>().AddEffect(content, "paper_russtle");
            background.GetComponent<AudioPlayer>().AddSong(content, "Lullaby");

            var infoText = GameObject.CreateNew("info", new Vector2(20), new Vector2(ScreenHandler.Width - 40, 800));
            infoText.AddComponent<Text2D>().Text = "Select difficulty";
            infoText.GetComponent<Text2D>().Align = Text2D.eAlignment.Center;

            AddObjectsRange(new List<GameObject>
            {
                background, infoText
            });
            #endregion

            #region Difficulty
            var maleButton = GameObject.CreateTemplate("difficulty_male", GameObject.Templates.Button, new Vector2(ScreenHandler.Width / 2 - ScreenHandler.Width / 2, 550), new Vector2(ScreenHandler.Width, 40));
            maleButton.GetComponent<Text2D>().Text = "Male";
            maleButton.GetComponent<Button>().SetVisibility(false);
            maleButton.GetComponent<Button>().OnClick += Difficulty_Selection;
            maleButton.AddComponent<TextButtonBehavior>();

            var femaleButton = GameObject.CreateTemplate("difficulty_female", GameObject.Templates.Button, new Vector2(ScreenHandler.Width / 2 - ScreenHandler.Width / 2, 550 + 40), new Vector2(ScreenHandler.Width, 40));
            femaleButton.GetComponent<Text2D>().Text = "Female";
            femaleButton.GetComponent<Button>().SetVisibility(false);
            femaleButton.GetComponent<Button>().OnClick += Difficulty_Selection;
            femaleButton.AddComponent<TextButtonBehavior>();

            var otherButton = GameObject.CreateTemplate("difficulty_other", GameObject.Templates.Button, new Vector2(ScreenHandler.Width / 2 - ScreenHandler.Width / 2, 550 + 40 * 2), new Vector2(ScreenHandler.Width, 40));
            otherButton.GetComponent<Text2D>().Text = "Other";
            otherButton.GetComponent<Button>().SetVisibility(false);
            otherButton.GetComponent<Button>().OnClick += Difficulty_Selection;
            otherButton.AddComponent<TextButtonBehavior>();

            var returnButton = GameObject.CreateTemplate("difficulty_return", GameObject.Templates.Button, new Vector2(ScreenHandler.Width / 2 - ScreenHandler.Width / 2, 550 + 40 * 5), new Vector2(ScreenHandler.Width, 40));
            returnButton.GetComponent<Text2D>().Text = "Return";
            returnButton.GetComponent<Button>().SetVisibility(false);
            returnButton.GetComponent<Button>().OnClick += Difficulty_Selection;
            returnButton.AddComponent<TextButtonBehavior>();

            AddObjectsRange(new List<GameObject>
            {
                maleButton, femaleButton, otherButton, returnButton
            });
            #endregion

            #region ChildHood
            var streetboy = GameObject.CreateTemplate("childhood_streets", GameObject.Templates.Button, new Vector2(ScreenHandler.Width / 2 - ScreenHandler.Width / 2, 550), new Vector2(ScreenHandler.Width, 40));
            streetboy.GetComponent<Text2D>().Text = "On the streets";
            streetboy.GetComponent<Button>().SetVisibility(false);
            streetboy.GetComponent<Button>().OnClick += ChildHood_Selection;
            streetboy.AddComponent<TextButtonBehavior>();
            streetboy.SetActive(false);

            var child_slave = GameObject.CreateTemplate("childhood_slave", GameObject.Templates.Button, new Vector2(ScreenHandler.Width / 2 - ScreenHandler.Width / 2, 550 + 40), new Vector2(ScreenHandler.Width, 40));
            child_slave.GetComponent<Text2D>().Text = "As a slave";
            child_slave.GetComponent<Button>().SetVisibility(false);
            child_slave.GetComponent<Button>().OnClick += ChildHood_Selection;
            child_slave.AddComponent<TextButtonBehavior>();
            child_slave.SetActive(false);

            var serf = GameObject.CreateTemplate("childhood_serf", GameObject.Templates.Button, new Vector2(ScreenHandler.Width / 2 - ScreenHandler.Width / 2, 550 + 40 * 2), new Vector2(ScreenHandler.Width, 40));
            serf.GetComponent<Text2D>().Text = "In a serfdom";
            serf.GetComponent<Button>().SetVisibility(false);
            serf.GetComponent<Button>().OnClick += ChildHood_Selection;
            serf.AddComponent<TextButtonBehavior>();
            serf.SetActive(false);

            var townsman = GameObject.CreateTemplate("childhood_townsman", GameObject.Templates.Button, new Vector2(ScreenHandler.Width / 2 - ScreenHandler.Width / 2, 550 + 40 * 3), new Vector2(ScreenHandler.Width, 40));
            townsman.GetComponent<Text2D>().Text = "Living in a town";
            townsman.GetComponent<Button>().SetVisibility(false);
            townsman.GetComponent<Button>().OnClick += ChildHood_Selection;
            townsman.AddComponent<TextButtonBehavior>();
            townsman.SetActive(false);

            var aclodyte = GameObject.CreateTemplate("childhood_aclodyte", GameObject.Templates.Button, new Vector2(ScreenHandler.Width / 2 - ScreenHandler.Width / 2, 550 + 40 * 4), new Vector2(ScreenHandler.Width, 40));
            aclodyte.GetComponent<Text2D>().Text = "Raised by the church";
            aclodyte.GetComponent<Button>().SetVisibility(false);
            aclodyte.GetComponent<Button>().OnClick += ChildHood_Selection;
            aclodyte.AddComponent<TextButtonBehavior>();
            aclodyte.SetActive(false);

            var noble = GameObject.CreateTemplate("childhood_noble", GameObject.Templates.Button, new Vector2(ScreenHandler.Width / 2 - ScreenHandler.Width / 2, 550 + 40 * 5), new Vector2(ScreenHandler.Width, 40));
            noble.GetComponent<Text2D>().Text = "At a nobles court";
            noble.GetComponent<Button>().SetVisibility(false);
            noble.GetComponent<Button>().OnClick += ChildHood_Selection;
            noble.AddComponent<TextButtonBehavior>();
            noble.SetActive(false);

            var nvm_childhood = GameObject.CreateTemplate("childhood_nvm", GameObject.Templates.Button, new Vector2(ScreenHandler.Width / 2 - ScreenHandler.Width / 2, 550 + 40 * 7), new Vector2(ScreenHandler.Width, 40));
            nvm_childhood.GetComponent<Text2D>().Text = "When I think about it...";
            nvm_childhood.GetComponent<Button>().SetVisibility(false);
            nvm_childhood.GetComponent<Button>().OnClick += ChildHood_Selection;
            nvm_childhood.AddComponent<TextButtonBehavior>();
            nvm_childhood.SetActive(false);

            AddObjectsRange(new List<GameObject>
            {
                streetboy, child_slave, serf, townsman, aclodyte, noble, nvm_childhood
            });
            #endregion

            #region AdultLife
            var beggar = GameObject.CreateTemplate("adultlife_beggar", GameObject.Templates.Button, new Vector2(ScreenHandler.Width / 2 - ScreenHandler.Width / 2, 550), new Vector2(ScreenHandler.Width, 40));
            beggar.GetComponent<Text2D>().Text = "As a beggar";
            beggar.GetComponent<Button>().SetVisibility(false);
            beggar.GetComponent<Button>().OnClick += AdultLife_Selection;
            beggar.AddComponent<TextButtonBehavior>();
            beggar.SetActive(false);

            var adult_slave = GameObject.CreateTemplate("adultlife_slave", GameObject.Templates.Button, new Vector2(ScreenHandler.Width / 2 - ScreenHandler.Width / 2, 550 + 40), new Vector2(ScreenHandler.Width, 40));
            adult_slave.GetComponent<Text2D>().Text = "Being a slave";
            adult_slave.GetComponent<Button>().SetVisibility(false);
            adult_slave.GetComponent<Button>().OnClick += AdultLife_Selection;
            adult_slave.AddComponent<TextButtonBehavior>();
            adult_slave.SetActive(false);

            var farmer = GameObject.CreateTemplate("adultlife_farmer", GameObject.Templates.Button, new Vector2(ScreenHandler.Width / 2 - ScreenHandler.Width / 2, 550 + 40 * 2), new Vector2(ScreenHandler.Width, 40));
            farmer.GetComponent<Text2D>().Text = "Working on a farm";
            farmer.GetComponent<Button>().SetVisibility(false);
            farmer.GetComponent<Button>().OnClick += AdultLife_Selection;
            farmer.AddComponent<TextButtonBehavior>();
            farmer.SetActive(false);

            var trader = GameObject.CreateTemplate("adultlife_trader", GameObject.Templates.Button, new Vector2(ScreenHandler.Width / 2 - ScreenHandler.Width / 2, 550 + 40 * 3), new Vector2(ScreenHandler.Width, 40));
            trader.GetComponent<Text2D>().Text = "Trading with folks";
            trader.GetComponent<Button>().SetVisibility(false);
            trader.GetComponent<Button>().OnClick += AdultLife_Selection;
            trader.AddComponent<TextButtonBehavior>();
            trader.SetActive(false);

            var crafter = GameObject.CreateTemplate("adultlife_crafter", GameObject.Templates.Button, new Vector2(ScreenHandler.Width / 2 - ScreenHandler.Width / 2, 550 + 40 * 4), new Vector2(ScreenHandler.Width, 40));
            crafter.GetComponent<Text2D>().Text = "Crafting and smithing";
            crafter.GetComponent<Button>().SetVisibility(false);
            crafter.GetComponent<Button>().OnClick += AdultLife_Selection;
            crafter.AddComponent<TextButtonBehavior>();
            crafter.SetActive(false);

            var student = GameObject.CreateTemplate("adultlife_student", GameObject.Templates.Button, new Vector2(ScreenHandler.Width / 2 - ScreenHandler.Width / 2, 550 + 40 * 5), new Vector2(ScreenHandler.Width, 40));
            student.GetComponent<Text2D>().Text = "Studying at a university";
            student.GetComponent<Button>().SetVisibility(false);
            student.GetComponent<Button>().OnClick += AdultLife_Selection;
            student.AddComponent<TextButtonBehavior>();
            student.SetActive(false);

            var courtier = GameObject.CreateTemplate("adultlife_courtier", GameObject.Templates.Button, new Vector2(ScreenHandler.Width / 2 - ScreenHandler.Width / 2, 550 + 40 * 6), new Vector2(ScreenHandler.Width, 40));
            courtier.GetComponent<Text2D>().Text = "Living as a courtier";
            courtier.GetComponent<Button>().SetVisibility(false);
            courtier.GetComponent<Button>().OnClick += AdultLife_Selection;
            courtier.AddComponent<TextButtonBehavior>();
            courtier.SetActive(false);

            var nvm_adultlife = GameObject.CreateTemplate("adultlife_nvm", GameObject.Templates.Button, new Vector2(ScreenHandler.Width / 2 - ScreenHandler.Width / 2, 550 + 40 * 8), new Vector2(ScreenHandler.Width, 40));
            nvm_adultlife.GetComponent<Text2D>().Text = "When I think about it...";
            nvm_adultlife.GetComponent<Button>().SetVisibility(false);
            nvm_adultlife.GetComponent<Button>().OnClick += AdultLife_Selection;
            nvm_adultlife.AddComponent<TextButtonBehavior>();
            nvm_adultlife.SetActive(false);

            AddObjectsRange(new List<GameObject>
            {
                beggar, adult_slave, farmer, trader, crafter, student, courtier, nvm_adultlife
            });
            #endregion

            #region Motivation
            var justice = GameObject.CreateTemplate("motivation_justice", GameObject.Templates.Button, new Vector2(ScreenHandler.Width / 2 - ScreenHandler.Width / 2, 550), new Vector2(ScreenHandler.Width, 40));
            justice.GetComponent<Text2D>().Text = "Justice";
            justice.GetComponent<Button>().SetVisibility(false);
            justice.GetComponent<Button>().OnClick += Motivation_Selection;
            justice.AddComponent<TextButtonBehavior>();
            justice.SetActive(false);

            var hope = GameObject.CreateTemplate("motivation_hope", GameObject.Templates.Button, new Vector2(ScreenHandler.Width / 2 - ScreenHandler.Width / 2, 550 + 40), new Vector2(ScreenHandler.Width, 40));
            hope.GetComponent<Text2D>().Text = "Hope";
            hope.GetComponent<Button>().SetVisibility(false);
            hope.GetComponent<Button>().OnClick += Motivation_Selection;
            hope.AddComponent<TextButtonBehavior>();
            hope.SetActive(false);

            var religion = GameObject.CreateTemplate("motivation_religion", GameObject.Templates.Button, new Vector2(ScreenHandler.Width / 2 - ScreenHandler.Width / 2, 550 + 40 * 2), new Vector2(ScreenHandler.Width, 40));
            religion.GetComponent<Text2D>().Text = "Religion";
            religion.GetComponent<Button>().SetVisibility(false);
            religion.GetComponent<Button>().OnClick += Motivation_Selection;
            religion.AddComponent<TextButtonBehavior>();
            religion.SetActive(false);

            var revenge = GameObject.CreateTemplate("motivation_revenge", GameObject.Templates.Button, new Vector2(ScreenHandler.Width / 2 - ScreenHandler.Width / 2, 550 + 40 * 3), new Vector2(ScreenHandler.Width, 40));
            revenge.GetComponent<Text2D>().Text = "Revenge";
            revenge.GetComponent<Button>().SetVisibility(false);
            revenge.GetComponent<Button>().OnClick += Motivation_Selection;
            revenge.AddComponent<TextButtonBehavior>();
            revenge.SetActive(false);

            var nvm_motivation = GameObject.CreateTemplate("motivation_nvm", GameObject.Templates.Button, new Vector2(ScreenHandler.Width / 2 - ScreenHandler.Width / 2, 550 + 40 * 5), new Vector2(ScreenHandler.Width, 40));
            nvm_motivation.GetComponent<Text2D>().Text = "When I think about it...";
            nvm_motivation.GetComponent<Button>().SetVisibility(false);
            nvm_motivation.GetComponent<Button>().OnClick += Motivation_Selection;
            nvm_motivation.AddComponent<TextButtonBehavior>();
            nvm_motivation.SetActive(false);

            AddObjectsRange(new List<GameObject>
            {
                justice, hope, religion, revenge, nvm_motivation
            });
            #endregion

            #region Accept/Decline
            var Accept = GameObject.CreateTemplate("btn_accept", GameObject.Templates.Button, new Vector2(ScreenHandler.Width / 2 - ScreenHandler.Width / 2, 550), new Vector2(ScreenHandler.Width, 40));
            Accept.GetComponent<Text2D>().Text = "Continue...";
            Accept.GetComponent<Button>().SetVisibility(false);
            Accept.GetComponent<Button>().OnClick += Confirmation;
            Accept.AddComponent<TextButtonBehavior>();
            Accept.SetActive(false);

            var Decline = GameObject.CreateTemplate("btn_decline", GameObject.Templates.Button, new Vector2(ScreenHandler.Width / 2 - ScreenHandler.Width / 2, 550 + 40), new Vector2(ScreenHandler.Width, 40));
            Decline.GetComponent<Text2D>().Text = "I've changed my mind...";
            Decline.GetComponent<Button>().SetVisibility(false);
            Decline.GetComponent<Button>().OnClick += Confirmation;
            Decline.AddComponent<TextButtonBehavior>();
            Decline.SetActive(false);

            AddObjectsRange(new List<GameObject>
            {
                Accept, Decline
            });
            #endregion
        }

        private void Confirmation(GameObject gameObject)
        {
            bool accepted = false;
            if (gameObject.Name.EndsWith("accept")) accepted = true;

            var obj = GetObjectByName("BackgroundImage");

            if (obj != null && obj.HasComponent<AudioPlayer>())
            {
                obj.GetComponent<AudioPlayer>().PlayEffect("paper_russtle");
            }

            switch (_currentMenu)
            {
                case Menues.Difficulty:
                    if (accepted)
                    {
                        ShowMenu(Menues.ChildHood);
                    }
                    else
                    {
                        ShowMenu(Menues.Difficulty);
                    }
                    break;

                case Menues.ChildHood:
                    if (accepted)
                    {
                        ShowMenu(Menues.AdultLife);
                    }
                    else
                    {
                        ShowMenu(Menues.ChildHood);
                    }
                    break;

                case Menues.AdultLife:
                    if (accepted)
                    {
                        ShowMenu(Menues.Motivation);
                    }
                    else
                    {
                        ShowMenu(Menues.AdultLife);
                    }
                    break;

                case Menues.Motivation:
                    if (accepted)
                    {
                        //TODO: GENERATE CHARACTER WITH STATS

                        //Create stats for character
                        //Make character with stats
                        //Save character
                        //Load Lobby Level
                        SceneManager.LoadScene("Village");
                    }
                    else
                    {
                        ShowMenu(Menues.Motivation);
                    }
                    break;

                default:
                    GetObjectByName("info").GetComponent<Text2D>().Text = "" +
                        "UNKNOWN_ERROR_AT_CONFIRMATION_METHOD" +
                        "\n " +
                        "\n " +
                        "Press § to open console. (under escape)";
                    GetObjectByName("btn_accept").SetActive(false);
                    GetObjectByName("btn_decline").SetActive(false);

                    //Create button to forcfully quit the game.
                    var forced_quit_button = GameObject.CreateTemplate("btn_forceQuit", GameObject.Templates.Button, new Vector2(ScreenHandler.Width / 2 - 100, ScreenHandler.Height / 2));
                    forced_quit_button.GetComponent<Button>().OnClick += FORCED_QUIT;
                    forced_quit_button.GetComponent<Button>().TintSetting.Normal = Color.Red;
                    forced_quit_button.GetComponent<Button>().TintSetting.Hover = Color.DarkRed;
                    forced_quit_button.GetComponent<Button>().TintSetting.Pressed = Color.DarkRed;
                    forced_quit_button.GetComponent<Text2D>().Text = "Quit Game";
                    AddObject(forced_quit_button);

                    GameConsole.PrintLine("!!!MISSING IMPLEMENTATION ERROR!!!", Color.Red);
                    GameConsole.PrintLine($">> {_currentMenu.ToString()}-Menu could not show next menu");
                    break;
            }
        }

        /// <summary>
        /// Called in case something went wrong during the selection screens.
        /// </summary>
        /// <param name="gameObject"></param>
        private void FORCED_QUIT(GameObject gameObject)
        {
            game.Quit();
        }

        private void ShowMenu(Menues nextMenu)
        {
            //Hides previous menu
            var previousMenuItems = GetObjectsByName(_currentMenu.ToString().ToLower() + "_%");

            foreach (var item in previousMenuItems)
            {
                item.SetActive(false);
            }

            //Hides confirmation menu
            GetObjectByName("btn_accept").SetActive(false);
            GetObjectByName("btn_decline").SetActive(false);

            //Changes the menu
            if (nextMenu != Menues.Confirmation)
            {
                _currentMenu = nextMenu;

                //Displays the requested menu
                var objects = GetObjectsByName(_currentMenu.ToString().ToLower() + "_%");

                foreach (var item in objects)
                {
                    item.SetActive(true);
                }
            }

            var message = GetObjectByName("info").GetComponent<Text2D>();

            //Changes menu specific text
            switch (nextMenu)
            {
                case Menues.Difficulty:
                    message.Text = "Select difficulty";
                    break;

                case Menues.ChildHood:
                    if (_difficulty == Difficulty.Male)
                        message.Text = "You were born a male, capable of taking your life in your own hands.\n";

                    if (_difficulty == Difficulty.Female)
                        message.Text = "Being female not many took you serious throughout your life.";

                    if (_difficulty == Difficulty.Enuch)
                        message.Text = "Life was always harsh on you, no matter what you did.";

                    message.Text += "\nAs soon as you knew how to walk you spent your childhood...";
                    break;

                case Menues.AdultLife:
                    message.Text = "adult_life_description";
                    break;

                case Menues.Motivation:
                    message.Text = "motivation_description";
                    break;

                case Menues.Confirmation:
                    GetObjectByName("btn_accept").SetActive(true);
                    GetObjectByName("btn_decline").SetActive(true);
                    break;
            }
        }

        private void Motivation_Selection(GameObject gameObject)
        {
            var obj = GetObjectByName("BackgroundImage");

            if (gameObject.Name.EndsWith("justice"))
            {
                _motivation = Motivation.Justice;
            }
            else if (gameObject.Name.EndsWith("hope"))
            {
                _motivation = Motivation.Hope;
            }
            else if (gameObject.Name.EndsWith("religion"))
            {
                _motivation = Motivation.Religion;
            }
            else if (gameObject.Name.EndsWith("revenge"))
            {
                _motivation = Motivation.Revenge;
            }
            else
            {
                ShowMenu(Menues.AdultLife);

                if (obj != null && obj.HasComponent<AudioPlayer>())
                {
                    obj.GetComponent<AudioPlayer>().PlayEffect("paper_russtle");
                }

                return; //Returns out of this method
            }

            if (obj != null && obj.HasComponent<AudioPlayer>())
            {
                obj.GetComponent<AudioPlayer>().PlayEffect("pensil");
            }

            var message = GetObjectByName("info").GetComponent<Text2D>();

            switch (_adultLife)
            {
                default:
                    message.Text = _motivation.ToString();
                    break;
            }

            ShowMenu(Menues.Confirmation);
        }

        private void AdultLife_Selection(GameObject gameObject)
        {
            var obj = GetObjectByName("BackgroundImage");

            if (gameObject.Name.EndsWith("beggar"))
            {
                _adultLife = AdultLife.Beggar;
            }
            else if (gameObject.Name.EndsWith("slave"))
            {
                _adultLife = AdultLife.Slave;
            }
            else if (gameObject.Name.EndsWith("farmer"))
            {
                _adultLife = AdultLife.Farmer;
            }
            else if (gameObject.Name.EndsWith("trader"))
            {
                _adultLife = AdultLife.Trader;
            }
            else if (gameObject.Name.EndsWith("crafter"))
            {
                _adultLife = AdultLife.Crafter;
            }
            else if (gameObject.Name.EndsWith("student"))
            {
                _adultLife = AdultLife.Student;
            }
            else if (gameObject.Name.EndsWith("noble"))
            {
                _adultLife = AdultLife.Courtier;
            }
            else
            {
                ShowMenu(Menues.ChildHood);

                if (obj != null && obj.HasComponent<AudioPlayer>())
                {
                    obj.GetComponent<AudioPlayer>().PlayEffect("paper_russtle");
                }

                return; //Returns out of this method
            }

            if (obj != null && obj.HasComponent<AudioPlayer>())
            {
                obj.GetComponent<AudioPlayer>().PlayEffect("pensil");
            }

            var message = GetObjectByName("info").GetComponent<Text2D>();

            switch (_adultLife)
            {
                default:
                    message.Text = _adultLife.ToString();
                    break;
            }

            ShowMenu(Menues.Confirmation);
        }

        private void ChildHood_Selection(GameObject gameObject)
        {
            var obj = GetObjectByName("BackgroundImage");

            if (gameObject.Name.EndsWith("streets"))
            {
                _childHood = ChildHood.Streets;
            }
            else if (gameObject.Name.EndsWith("slave"))
            {
                _childHood = ChildHood.Slave;
            }
            else if (gameObject.Name.EndsWith("serf"))
            {
                _childHood = ChildHood.Serf;
            }
            else if (gameObject.Name.EndsWith("townsman"))
            {
                _childHood = ChildHood.Townsfolk;
            }
            else if (gameObject.Name.EndsWith("aclodyte"))
            {
                _childHood = ChildHood.Clergy;
            }
            else if (gameObject.Name.EndsWith("noble"))
            {
                _childHood = ChildHood.Nobility;
            }
            else
            {
                ShowMenu(Menues.Difficulty);

                if (obj != null && obj.HasComponent<AudioPlayer>())
                {
                    obj.GetComponent<AudioPlayer>().PlayEffect("paper_russtle");
                }

                return; //Returns out of this method
            }

            if (obj != null && obj.HasComponent<AudioPlayer>())
            {
                obj.GetComponent<AudioPlayer>().PlayEffect("pensil");
            }

            var message = GetObjectByName("info").GetComponent<Text2D>();

            switch (_childHood)
            {
                case ChildHood.Streets:
                    message.Text = "As a child on the streets you spent many nights hungry and cold.\nLviving the life without money you quickly learnt how to make people share what they've got.\nOr in case they wouldn't share, you knew how to take what you needed for living without beeing seen...";
                    break;

                case ChildHood.Slave:
                    message.Text = "Born into slavery you spend all your days working being ordered going here and and doing things there.\nAs a slave you learnt how to together with your team make the best out of the situation, taking turns resting and hiding the fact from the slaver.\nAs a heavy worker your stamina was greatly superior to that of those living the life.";
                    break;

                default:
                    message.Text = _childHood.ToString();
                    break;
            }

            ShowMenu(Menues.Confirmation);
        }

        private void Difficulty_Selection(GameObject gameObject)
        {
            var obj = GetObjectByName("BackgroundImage");

            if (gameObject.Name.EndsWith("female"))
            {
                _difficulty = Difficulty.Female;
            }
            else if (gameObject.Name.EndsWith("male"))
            {
                _difficulty = Difficulty.Male;
            }
            else if (gameObject.Name.EndsWith("other"))
            {
                _difficulty = Difficulty.Enuch;
            }
            else
            {
                if (obj != null && obj.HasComponent<AudioPlayer>())
                {
                    obj.GetComponent<AudioPlayer>().PlayEffect("paper_russtle");
                }

                SceneManager.LoadScene("MainMenu");
                return;
            }

            if (obj != null && obj.HasComponent<AudioPlayer>())
            {
                obj.GetComponent<AudioPlayer>().PlayEffect("pensil");
            }

            switch (_difficulty)
            {
                case Difficulty.Male:
                    GetObjectByName("info").GetComponent<Text2D>().Text = "Born a male, your life will propably be more fair.";
                    break;

                case Difficulty.Female:
                    GetObjectByName("info").GetComponent<Text2D>().Text = "Born a female, your life might be strugglesome.";
                    break;

                case Difficulty.Enuch:
                    GetObjectByName("info").GetComponent<Text2D>().Text = "Born an enuch, your life will be very difficult.";
                    break;
            }

            ShowMenu(Menues.Confirmation);
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            base.Draw(gameTime, spriteBatch);
        }

        public override void PostUpdate(GameTime gameTime)
        {
            base.PostUpdate(gameTime);
        }

        public override void Start()
        {
            base.Start();

            MediaPlayer.ActiveSongChanged += MediaPlayer_ActiveSongChanged;
        }

        private void MediaPlayer_ActiveSongChanged(object sender, System.EventArgs e)
        {
            GetObjectByName("BackgroundImage").GetComponent<AudioPlayer>().PlaySong("Lullaby");
        }
        
        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }
    }
}
